#version 330 core

layout (location = 0) out vec4 vFragColor;


uniform int fTextured;
uniform int fOverrideAlpha;
uniform int fPixelsCoordinates;
uniform int fPixelsFlipped;

uniform sampler2D tSampler;

smooth in vec2 vUV;
in vec4 fColor;



void main()
{
	 vec2 vUVc = vUV;
	 if(fTextured == 1)
	 {
	 if(fPixelsFlipped == 1)
	 {
		vUVc = vUV * vec2(1.0,-1.0);
	 }
	
	 if(fPixelsCoordinates == 1)
	 {
		 ivec2 siz = textureSize(tSampler, 0);
		 vUVc = vUV / siz;
	 }
	
	 vec4 samplTex = texture(tSampler, vUVc.st);
	
	 if(samplTex.a < 0.01f)
		 discard;
		
	vec4 clr = samplTex * vec4(fColor);

	vFragColor = clr;
	
	
	
	 }	
	 else
	 {
		vFragColor = vec4(fColor);
	 }

	
}

