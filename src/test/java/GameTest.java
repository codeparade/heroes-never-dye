package Tests;


import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertThat;
import Engine.Renderer.Sprite;
import Engine.System.Vector2Df;
import Engine.Renderer.Window;
import Engine.Renderer.VideoSettings;
import Engine.Renderer.Color;
import static org.hamcrest.CoreMatchers.instanceOf;
import Engine.Renderer.Camera;
import Engine.Renderer.Text;

public class GameTest {

    @Test
    public void spriteTransformTest() {

        //OpenGL Initialization
        Window win = new Window(new VideoSettings(1,1,32), "Test");
        Sprite s = new Sprite();
        s.move(10,0);

        Assert.assertEquals(s.getPosition().length(),new Vector2Df(10.f,0.f).length(),0.01);
    }

    @Test
    public void colorTest() {
       Assert.assertEquals(Color.Transparent.a, 0.0f, 0.0001f); //Transparent color should have alpha = 0
    }

    @Test
    public void cameraTest() {
        Camera c = new Camera(new Vector2Df(100.f, 100.f), new Vector2Df(100.f, 100.f));

        c.rotate(10.f);
        c.move(10.f, 0.f);

        float[] matrix = c.getTransform().getMatrix();

        float[] base = new float[] {  0.02f, 0.00f, 0.f, -2.51f,
                                        0.00f, -0.02f, 0.f, 1.59f,
                                        0.f, 0.f, 1.f, 0.f,
                                        0.f, 0.f, 0.f, 1.f};
        Assert.assertArrayEquals(matrix, base, 0.1f);
    }

    @Test
    public void TextTest()
    {
        Text t = new Text();
        t.setText("test");
        Assert.assertEquals(t.getString(), "test");
    }



}