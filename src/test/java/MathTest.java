package Tests;

import junit.framework.Assert;
import org.junit.Test;
import Engine.System.Vector2Df;
import Engine.System.Vector2Di;



public class MathTest {

    @Test
    public void testVector2DLentgth() {

        Vector2Df v = new Vector2Df(2.f, 2.f);
        Assert.assertEquals(2.8284, v.length(), 0.01f);
    }

    @Test
    public void testVector2DiLentgth() {

        Vector2Di v = new Vector2Di(4, 4);
        Assert.assertEquals(5.65685, v.length(), 0.01f);
    }

}