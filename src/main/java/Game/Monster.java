package Game;

import Engine.System.Vector2Df;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public abstract class Monster extends Moveable {
    public static int monsterNumber = 0;
    private Random rand = new Random();
    private Vector2Df p = new Vector2Df(Player.getInstance().getPosition().x, Player.getInstance().getPosition().y);
    protected boolean isDead;

    abstract protected void changeSprite(State state);
    abstract public void moved();

    public void moved(float horizontalSpeed, float jumpDist, double jumpProbability) {
        if (spriteNr == 0 || state == State.WALK)
            animationEnded = false;

        p.x = Player.getInstance().getPosition().x;
        p.y = Player.getInstance().getPosition().y;

        fallIfAboveGround();

        if (p.x < getPosition().x && rand.nextDouble() > 0.2) {
            move(-horizontalSpeed, 0.f);
            aPressed = true;
            this.flipHorizontally(true);
        }

        if (p.x > getPosition().x && rand.nextDouble() > 0.2) {
            move(horizontalSpeed, 0.f);
            dPressed = false;
            this.flipHorizontally(false);
        }

        if (rand.nextDouble() > 1 - jumpProbability) {
            inAir++;
            if (inAir <= 2) {
                move(0, jumpDist);
            }
        }


        isCloseEnoughToAttack();


        spriteNr++;
        if (spriteNr > 3) {
            spriteNr = 0;
            animationEnded = true;
        }
    }

    private void fallIfAboveGround() {
        if (getPosition().y != levelPos) {
            if (getPosition().y - 30f < levelPos) {
                setPosition(getPosition().x, levelPos);
            } else {
                if (aPressed) {
                    move(-70f, -30f);
                } else if (dPressed) {
                    move(70f, -30f);
                } else
                    move(0, -30f);
            }
        } else {
            inAir = 0;
            aPressed = false;
            dPressed = false;
        }
    }

    private void isCloseEnoughToAttack() {
        x_diff = Math.abs(p.x - getPosition().x);
        y_diff = Math.abs(p.y - getPosition().y);
        // mechanism should choose attack state depending on distance from player;
        if (Math.sqrt(x_diff * x_diff + y_diff * y_diff) < 60f) {
            attack(State.ATTACK1);
        } else if (Math.sqrt(x_diff * x_diff + y_diff * y_diff) < 220f) {
            attack(State.ATTACK2);
        } else if (Math.sqrt(x_diff * x_diff + y_diff * y_diff) < 320f) {
            attack(State.ATTACK3);
        } else {
            changeSprite(State.WALK);
        }
    }


    protected void attack(State state) {
        Map<String, Double> m_map = new HashMap<>();
        if (state == State.ATTACK1)
            m_map = attackStats.get(0);

        else if (state == State.ATTACK2)
            m_map = attackStats.get(1);

        else if (state == State.ATTACK3)
            m_map = attackStats.get(2);

        dmg = m_map.get("dmg");
        attackDist = m_map.get("attackDist");
        attackChance = m_map.get("attackChance");

        inAir++;
        if (inAir <= 1) {
            move(10f, 50f);
        }

        if (this.state != state && !animationEnded) {
            this.state = state;
            spriteNr = 0;
            if (rand.nextDouble() > 1 - attackChance) {
                dmg = getAttack()*dmg - Player.getInstance().getArmor();
                if (dmg <= 0)
                    dmg = 2;
                Player.getInstance().changeHP(-dmg);
            }
        }
        changeSprite(state);

    }

}

