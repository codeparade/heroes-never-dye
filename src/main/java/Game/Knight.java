package Game;

import java.awt.*;
import java.util.*;

public class Knight extends Monster {
    public Knight() {
        super();
        monsterNumber++;
        height = 90;
        width = 82;
        isDead = false;
        setName("Knight");
        setAttack(8);
        setArmor(30);
        setHp(300);
        levelPos += height;
        state = State.WALK;

        setMap(mapForAttack1, 5., 80., 0.8);
        attackStats.add(mapForAttack1);

        setMap(mapForAttack2, 8., 180., 0.7);
        attackStats.add(mapForAttack2);

        setMap(mapForAttack3, 11., 200., 0.3);
        attackStats.add(mapForAttack3);

        Random rand = new Random();
        float distance = rand.nextFloat() * 20000;
        setPosition(distance, levelPos);
        setTexture(ResourceManager.getInstance().getTexture("monster-knight-walk"));
        setTextureRect(new Rectangle(75, 35, width, height));
    }

    protected void changeSprite(State state) {
        this.state = state;

        switch (state) {
            case WALK:
                setTexture(ResourceManager.getInstance().getTexture("monster-knight-walk"));
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(75, 35, width, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(75, 196, width, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(75, 360, width, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(75, 35, width, height));
                        break;
                }
                break;
            case ATTACK1:
                setTexture(ResourceManager.getInstance().getTexture("monster-knight-upstab"));
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(75, 0, 120, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(47, 206, width, 120));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(47, 368, width, 120));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(75, 688, width+10, height));
                        break;
                }
                break;
            case ATTACK2:
                setTexture(ResourceManager.getInstance().getTexture("monster-knight-stab"));
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(75, 0, 120, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(75, 212, 120, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(75, 380, 120, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(75, 540, width+10, height));
                        break;
                }
                break;
            case ATTACK3:
                setTexture(ResourceManager.getInstance().getTexture("monster-knight-upstab"));
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(75, 0, 120, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(75, 212, 120, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(75, 380, 120, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(75, 540, width+10, height));
                        break;
                }
                break;
            case DEAD:
                setTexture(ResourceManager.getInstance().getTexture("monster-knight-dead"));
                switch(spriteNr) {
                    case 0:
                    setTextureRect(new Rectangle(87, 20, width, height));
                    break;
                    case 1:
                    setTextureRect(new Rectangle(87, 189, width, height));
                    break;
                    case 2:
                    setTextureRect(new Rectangle(60, 345, width, height + 15));
                    break;
                    case 3:
                    setTextureRect(new Rectangle(60, 515, width, height + 15));
                    break;
                    case 4:
                    setTextureRect(new Rectangle(60, 676, width, height + 15));
                    break;
                    case 5:
                    setTextureRect(new Rectangle(60, 850, width, height + 15));
                    break;
                    case 6:
                    setTextureRect(new Rectangle(87, 1188, width, height));
                    break;
                }
                break;
        }
    }

    public void moved() {
        if (getHp() <= 0) {
            if (!isDead) {
                monsterNumber--;
                spriteNr = 0;
                isDead = true;
                setPosition(this.getPosition().x, levelPos);
                changeSprite(State.DEAD);
            } else {
                if (spriteNr < 6) {
                    spriteNr++;
                    changeSprite(State.DEAD);
                }
            }
        }
        if (!isDead) {
                moved(20f, 10f, 0.05);
        }
    }
}
