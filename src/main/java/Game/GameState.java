package Game;

/**
 * Game State
 */

public enum GameState {

    /**
     * Loading
     */

    Loading,

    /**
     * Main Menu
     */

    MainMenu,

    /**
     * Level loading
     */

    LevelLoading,

    /**
     * In Game
     */

    InGame,

    /**
     * Paused
     */

    Paused,

    /**
     * Unloading (On close)
     */

    Unloading,

    /**
     * Uninitialzied
     */

    Uninitialized
}
