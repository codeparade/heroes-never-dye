package Game;

import Engine.Input.BasicInput;
import Engine.Renderer.Camera;

public class Player extends Moveable {
    private static Player ourInstance = null;
    private int stateNr;
    private State state;
    private boolean canChangeState, isTurnRight;

    public static Player getInstance(String name) {
        if (ourInstance == null) {
            ourInstance = new Player(name);
        }
        return ourInstance;
    }

    public static Player getInstance() {
        return ourInstance;
    }

    private Player(String name) {
        super();
        state = State.STAND;
        stateNr = 1;
        canChangeState = true;
        isTurnRight = true;
        height = 157;
        width = 170;
        setName(name);
        setAttack(8);
        setArmor(40);
        setHp(1000);
        levelPos += height;
        setPosition(800f, levelPos);
        setTexture(ResourceManager.getInstance().getTexture("player-0001"));
    }

    public void moved(Camera m_defaultView) {
        changeState();

        changeTexture(m_defaultView);

        if(BasicInput.isKeyPressed(BasicInput.Keys.W, false)) {
           moved('w', m_defaultView);
        }

        if(BasicInput.isKeyPressed(BasicInput.Keys.A, false)) {
            moved('a', m_defaultView);
        }

        if(BasicInput.isKeyPressed(BasicInput.Keys.D, false)) {
            moved('d', m_defaultView);
        }

        if(BasicInput.isKeyPressed(BasicInput.Keys.F, false)) {
            moved('f', m_defaultView);
        }

        fallingDown(m_defaultView);
    }

    private void fallingDown(Camera m_defaultView) {
        if(getPosition().y != levelPos) {
            if (getPosition().y - 30f < levelPos) {
                setPosition(getPosition().x, levelPos);
            } else {
                if (aPressed) {
                    if(!BasicInput.isKeyPressed(BasicInput.Keys.A, false)) {
                        move(-100f, 0);
                        m_defaultView.move(-100.f, 0.f);
                    }
                }
                else if (dPressed) {
                    if (!BasicInput.isKeyPressed(BasicInput.Keys.D, false)) {
                        move(100f, 0);
                        m_defaultView.move(100.f, 0.f);
                    }
                }
                move(0, -30f);
            }
        } else {
            inAir = 0;
            aPressed = false;
            dPressed = false;
        }
    }

    private void changeState() {
        if (canChangeState) {
            if (inAir != 0) {
                if (this.state != State.JUMP) {
                    this.state = State.JUMP;
                    this.stateNr = 1;
                }
            } else if (BasicInput.isKeyPressed(BasicInput.Keys.A, false) || BasicInput.isKeyPressed(BasicInput.Keys.D, false)) {
                if (this.state != State.WALK) {
                    this.state = State.WALK;
                    this.stateNr = 1;
                }
            } else {
                if (this.state != State.STAND) {
                    this.state = State.STAND;
                    this.stateNr = 1;
                }
            }
        }
    }

    private void changeTexture(Camera m_defaultView) {
        String tex_name;

        switch (this.state) {
            case WALK:
                if (this.stateNr >= 37) {
                    this.stateNr = 1;
                }

                if (this.stateNr<10) {
                    tex_name = "player-000" + this.stateNr;
                }
                else {
                    tex_name = "player-00" + this.stateNr;
                }

                setTexture(ResourceManager.getInstance().getTexture(tex_name));

                this.stateNr+=4;
                break;
            case STAND:
                if (this.stateNr >= 18) {
                    this.stateNr = 1;
                }

                if (this.stateNr<10) {
                    tex_name = "wplayer-000" + this.stateNr;
                }
                else {
                    tex_name = "wplayer-00" + this.stateNr;
                }

                setTexture(ResourceManager.getInstance().getTexture(tex_name));

                this.stateNr+=1;
                break;
            case JUMP:
                tex_name = "jplayer-0000";
                setTexture(ResourceManager.getInstance().getTexture(tex_name));

                this.stateNr=1;
                break;
            case DASH:
                if (this.stateNr == 9)
                    canChangeState = true;
                tex_name = "dplayer-000" + this.stateNr;

                setTexture(ResourceManager.getInstance().getTexture(tex_name));

                float monsterDist[] = new float[(MonsterFactory.monsters).length];

                for (int i = 0; i < (MonsterFactory.monsters).length; i++) {
                    monsterDist[i] = getPosition().x - MonsterFactory.monsters[i].getPosition().x;
                }

                if (isTurnRight) {
                    move(140f, 0);
                    m_defaultView.move(140.f, 0.f);
                } else {
                    move(-140f, 0);
                    m_defaultView.move(-140.f, 0.f);
                }

                for (int i = 0; i < (MonsterFactory.monsters).length; i++) {
                    monsterDist[i] *= getPosition().x - MonsterFactory.monsters[i].getPosition().x;
                    if (monsterDist[i] < 0)
                        MonsterFactory.monsters[i].setHp(-1);
                }

                this.stateNr+=1;
                break;
        }

        if (isTurnRight) {
            this.flipHorizontally(false);
        } else {
            this.flipHorizontally(true);
        }
    }

    public void moved(char control, Camera m_defaultView) {
        if(control == 'w') {
            inAir++;
            if (inAir <= 2) {
                move(0, 130f);
            }
        }

        if(control == 'a') {
            move(-100f, 0);
            m_defaultView.move(-100.f, 0.f);
            isTurnRight = false;
            aPressed = true;
        }

        else if(control == 'd') {
            move(100f, 0);
            m_defaultView.move(100.f, 0.f);
            isTurnRight = true;
            dPressed = true;
        }

        if(control == 'f') {
            if (canChangeState) {
                state = State.DASH;
                stateNr = 1;
            }
            canChangeState = false;
        }
    }
}
