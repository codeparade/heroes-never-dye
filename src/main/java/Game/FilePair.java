package Game;

/**
 * Pair used to store Loading Entities
 */

public class FilePair {
    public String key;
    public String path;

    public FilePair(String k, String v) {
        key = k;
        path = v;
    }

}
