package Game;

import Engine.Renderer.Sprite;
import java.util.*;


public class Moveable extends Sprite {
    public enum State {
        WALK, ATTACK1, ATTACK2, ATTACK3, STAND, JUMP, DASH, DEAD
    }

    protected State state;

    protected int inAir = 0;
    protected boolean aPressed = false, dPressed = false;
    protected float levelPos = -757f;
    protected int height, width, temp_height, temp_width;
    protected int spriteNr = 0;

    protected boolean animationEnded = false;
    protected double x_diff, y_diff;

    private String name;
    private int hp;
    private int attack;
    private int armor;
    protected double dmg;
    protected double attackDist;
    protected double attackChance;
    protected List<Map<String, Double>> attackStats = new ArrayList<>();
    protected Map<String, Double>
            mapForAttack1 = new HashMap<>(),
            mapForAttack2 = new HashMap<>(),
            mapForAttack3 = new HashMap<>();

    private float m_xOffset = 0, m_yOffset = 0;

    protected void setMap(Map<String, Double> map, double attackDmg, double distance, double chance) {
        map.put("dmg", attackDmg);
        map.put("attackDist", distance);
        map.put("attackChance", chance);
    }

    public int getArmor() {
        return armor;
    }

    public int getAttack() {
        return attack;
    }

    public int getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public void setArmor(int armor) { this.armor = armor; }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void move(float xOffset, float yOffset){
        m_xOffset = xOffset;
        m_yOffset += yOffset;

        setPosition(getPosition().x + m_xOffset, getPosition().y + m_yOffset);

        if (getPosition().y < levelPos)
            setPosition(getPosition().x, levelPos);

        m_yOffset *= 0.7f * (-getPosition().y / 500f);
    }

    protected void changeHP(double hp) {
        setHp(this.hp + (int)hp);
    }
}


