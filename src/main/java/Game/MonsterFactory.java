package Game;

public class MonsterFactory {
    static public Monster monsters[];

    public Monster getMonster(String monsterType) {
        if (monsterType == null) {
            return new NullMonster();
        } else if (monsterType.equalsIgnoreCase("spider")) {
            return new Spider();
        } else if (monsterType.equalsIgnoreCase("knight")) {
            return new Knight();
        }
        return new NullMonster();
    }
}
