package Game;

public class NullMonster extends Monster {
    public NullMonster() {
        setName("Imaginary Monster");
        setAttack(0);
        setArmor(0);
        setHp(0);
    }

    public void changeSprite(State state) {
        System.out.println("You can't change state for monster that doesn't exist. Sorry!");
    }

    public void moved() {
        System.out.println("Imaginary monster does not move!");
    }
}
