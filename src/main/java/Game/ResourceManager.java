package Game;

import Engine.Audio.Sound;
import Engine.Renderer.Texture;


import java.io.*;
import java.util.*;

public class ResourceManager {

    private static ResourceManager ourInstance = new ResourceManager();

    public static ResourceManager getInstance() {
        return ourInstance;
    }

    private ResourceManager() {
        m_textures = new HashMap<String, Texture>();
        m_sounds = new HashMap<String, Sound>();
        m_texturesToLoad = new Stack<>();
        m_soundsToLoad = new Stack<>();
        m_loaderStatus = false;
    }

    public void loadTexture(String file, String name) {
        Texture t = new Texture();
        t.loadFromFile(file, null);

        if(m_textures.containsKey(name))
            throw new RuntimeException ("You are trying to load existing texture!");

        m_textures.put(name, t);
    }

    public void loadSound(String file, String name) {
        Sound s = new Sound();
        s.loadFromOgg(file);

        if(m_sounds.containsKey(name))
            throw new RuntimeException ("You are trying to load existing sound!");

        m_sounds.put(name, s);
    }

    public Texture getTexture(String name) {
        if(!m_textures.containsKey(name))
            throw new RuntimeException ("You are trying to get non-loaded texture!");

        return  m_textures.get(name);
    }

    public Sound getSound(String name) {
        if(!m_sounds.containsKey(name))
            throw new RuntimeException ("You are trying to get non-loaded sound!");

        return  m_sounds.get(name);
    }

    public synchronized void loader(String assetsAtlas) {

        try {
            Scanner in = new Scanner(new FileReader(assetsAtlas));

            while(in.hasNext()) {
                String[] a = in.nextLine().split("\\|");

                if(a.length != 3)
                    throw new RuntimeException("Problem with assets definition");

                FilePair f = new FilePair(a[1], a[2]);

                if(a[0].contains("T"))
                    m_texturesToLoad.push(f);
                else
                    m_soundsToLoad.push(f);
            }
            in.close();

        }
        catch (FileNotFoundException e ) {
            throw  new RuntimeException("Assets Atlas Not Found!");
        }


        Thread t = new Thread(() -> {

            try {
            Thread.sleep(1000);}
            catch (InterruptedException e) {}
            signalize();
        });

        t.start();

    }


    public void processLoader() {
        if(!m_texturesToLoad.empty()) {
            FilePair fp = m_texturesToLoad.pop();
            ResourceManager.getInstance().loadTexture(fp.path, fp.key);
        }
        else if(!m_soundsToLoad.empty()) {
            FilePair fp = m_soundsToLoad.pop();
            ResourceManager.getInstance().loadSound(fp.path, fp.key);
        }

    }


    public synchronized  void signalize() {
        m_loaderStatus = true;
    }


    public synchronized  boolean probeStatus() {
        return  m_loaderStatus && m_texturesToLoad.empty() && m_soundsToLoad.empty();
    }

    private HashMap<String, Texture> m_textures;
    private HashMap<String, Sound> m_sounds;

    private Stack<FilePair> m_texturesToLoad;
    private Stack<FilePair> m_soundsToLoad;


    private boolean m_loaderStatus;

}
