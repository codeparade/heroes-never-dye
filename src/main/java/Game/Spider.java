package Game;

import java.awt.*;
import java.util.*;

public class Spider extends Monster {
    public Spider() {
        super();
        monsterNumber++;
        height = 71;
        width = 86;
        setName("Spider");
        setAttack(15);
        setArmor(10);
        setHp(110);
        levelPos += height;
        state = State.WALK;

        setMap(mapForAttack1, 7., 120., 0.5);
        attackStats.add(mapForAttack1);

        setMap(mapForAttack2, 8., 150., 0.3);
        attackStats.add(mapForAttack2);

        setMap(mapForAttack3, 11., 170., 0.2);
        attackStats.add(mapForAttack3);

        Random rand = new Random();
        float distance = rand.nextFloat() * 20000;
        setPosition(distance, levelPos);
        setTexture(ResourceManager.getInstance().getTexture("monster-spider"));
        setTextureRect(new Rectangle(0, 0, width, height));
    }

    protected void changeSprite(State state) {
        this.state = state;
        switch (state) {
            case WALK:
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(0, 0, width, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(0, 150, width, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(0, 240, width, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(0, 337, width, height));
                        break;
                }
                break;
            case ATTACK1:
                switch (spriteNr) {
                    case 0:
                        temp_width = width;
                        width = 101;
                        setTextureRect(new Rectangle(147, 0, width, height));
                        width = temp_width;
                        break;
                    case 1:
                        temp_width = width;
                        width = 98;
                        setTextureRect(new Rectangle(147, 101, width, height));
                        width = temp_width;
                        break;
                    case 2:
                        temp_width = width;
                        width = 91;
                        setTextureRect(new Rectangle(147, 198, width, height));
                        width = temp_width;
                        break;
                    case 3:
                        temp_width = width;
                        width = 91;
                        setTextureRect(new Rectangle(147, 291, width, height));
                        width = temp_width;
                        break;
                }
                break;
            case ATTACK2:
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(76, 0, width, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(76, 164, width, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(76, 263, width, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(76, 364, width, height));
                        break;
                }
                break;
            case ATTACK3:
                switch (spriteNr) {
                    case 0:
                        setTextureRect(new Rectangle(0, 0, width, height));
                        break;
                    case 1:
                        setTextureRect(new Rectangle(0, 150, width, height));
                        break;
                    case 2:
                        setTextureRect(new Rectangle(0, 240, width, height));
                        break;
                    case 3:
                        setTextureRect(new Rectangle(0, 337, width, height));
                        break;
                }
                break;
            case DEAD:
                setTextureRect(new Rectangle(220, 382, width, height));
                break;
        }
    }

    public void moved() {
        if (!isDead) {
            if (getHp() <= 0) {
                monsterNumber--;
                isDead = true;
                setPosition(this.getPosition().x, levelPos);
                changeSprite(State.DEAD);
            } else {
                moved(30f, 20f, 0.1);
            }
        }
    }
}
