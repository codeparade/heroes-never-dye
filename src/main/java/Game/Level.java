package Game;

import Engine.Renderer.RenderTarget;
import Engine.Renderer.Sprite;
import Engine.Renderer.Texture;
import Engine.System.Vector2Df;

public class Level {

    public Level() {
        m_bg[0] = new Sprite();
        m_bg[1] = new Sprite();
        m_bg[2] = new Sprite();

        m_floor[0] = new Sprite();
        m_floor[1] = new Sprite();
        m_floor[2] = new Sprite();
    }

    public void setBackground(Texture bg) {
        m_bg[0].setTexture(bg);
        m_bg[1].setTexture(bg);
        m_bg[2].setTexture(bg);
    }



    public void setFloorTexture(Texture fl) {
        m_floor[0].setTexture(fl);
        m_floor[1].setTexture(fl);
        m_floor[2].setTexture(fl);
    }

    public void processPlayer(Vector2Df playerPos) {

        int block = (int)Math.floor(playerPos.x / 1920.0);

        if(block != m_currentBaseBlock) {
            m_currentBaseBlock = block;

            m_bg[0].setPosition((block - 1) * 1920, 0);
            m_bg[1].setPosition((block) * 1920, 0);
            m_bg[2].setPosition((block + 1) * 1920, 0);

            m_floor[0].setPosition((block - 1) * 1920, -650);
            m_floor[1].setPosition((block) * 1920, -650);
            m_floor[2].setPosition((block + 1) * 1920, -650);
        }
    }


    public void render(RenderTarget tgt) {
        tgt.draw(m_bg[0]);
        tgt.draw(m_bg[1]);
        tgt.draw(m_bg[2]);
        tgt.draw(m_floor[0]);
        tgt.draw(m_floor[1]);
        tgt.draw(m_floor[2]);
    }

    private Sprite[] m_bg = new Sprite[3];
    private Sprite[] m_floor = new Sprite[3];
    private int m_currentBaseBlock = -1;
}
