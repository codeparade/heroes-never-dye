package Game;

import Engine.Input.BasicInput;
import Engine.Renderer.*;
import Engine.Renderer.Color;
import Engine.Renderer.Window;
import Engine.System.Vector2Df;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import static Game.GameState.*;

public class Game {

    public Game() {
        m_state = GameState.Uninitialized;
        m_window = null;
        m_defaultView = null;
        m_splashSprite = null;
        m_splashTexture = null;
        m_player = null;
        m_currentLevel = null;
        ResourceManager.getInstance().loader("assets.dat");
        m_state = GameState.Loading;
    }

    public void init() {
        if (m_window == null) {
            m_window = new Window(new VideoSettings(1280, 720, 32), "Test");
            m_window.setClearColor(Color.Black);
        }
        m_defaultView = new Camera(new Vector2Df(1920,1080), new Vector2Df(960,540));
        m_defaultView.setViewport(new Rectangle2D.Float(0.f,0.f,((float)m_window.getSize().x), (float)(m_window.getSize().y)));

        m_splashTexture = new Texture();
        m_splashTexture.loadFromFile("splashTest.png", null);
        m_splashSprite = new Sprite();
        m_splashSprite.setTexture(m_splashTexture);
    }

    public void loop() {
        init();
        while(true) {
            BasicInput.processEvents();
            m_window.clearScreen();

            Text tx = new Text("", new Texture("font.png"));

            switch (m_state) {
                case Uninitialized:
                    throw new RuntimeException("You must initialize game before starting loop!");
                case Loading:
                    init();
                    m_window.setCamera(m_defaultView);
                    m_window.draw(m_splashSprite);

                    loadResources();
                    break;
                case MainMenu:
                    m_splashTexture.loadFromFile("splashTest.png", null);
                    m_splashSprite = new Sprite();
                    m_splashSprite.setTexture(m_splashTexture);
                    //Main Menu implementation

                    m_window.draw(m_mainMenuBg);
                    m_window.draw(m_mainMenuNg);
                    m_window.draw(m_mainMenuLg);
                    m_window.draw(m_mainMenuSt);
                    m_window.draw(m_mainMenuQt);

                    if(BasicInput.isMouseButtonPressed(BasicInput.MouseButton.Left)) {

                        Vector2Df pos = m_window.mapScreenPixelToRenderingCoords(BasicInput.getMousePosition(m_window));

                        //System.out.println(m_mainMenuNg.getBounds().toString());
                        //System.out.println(pos.x.toString() + ' ' + pos.y.toString());

                        if (m_mainMenuNg.getBounds().contains(pos.x,pos.y))
                            m_state = GameState.LevelLoading;

                    }

                   // m_state = GameState.LevelLoading;

                    break;
                case LevelLoading:
                    //First level - just for presentation
                    m_currentLevel = new Level();
                    m_currentLevel.setBackground(ResourceManager.getInstance().getTexture("bg-castle"));
                    m_currentLevel.setFloorTexture(ResourceManager.getInstance().getTexture("bg-floor"));

                    m_player = Player.getInstance("Gracz");
                    if (m_player.getHp() <= 0) {
                        m_player.setHp(1000);
                        m_player.setPosition(800f, -757);
                    }

                    MonsterFactory monsterFactory = new MonsterFactory();
                    MonsterFactory.monsters = new Monster[10];
                    String monsterType;
                    Random rand = new Random();
                    for(int i=0; i<10; i++) {
                        monsterType = (rand.nextInt(2) == 0) ? "knight" : "spider";
                        MonsterFactory.monsters[i] = monsterFactory.getMonster(monsterType);
                    }

                    m_hp_bar = new Sprite(ResourceManager.getInstance().getTexture("hp-bar"));
                    m_hp_bar_bg = new Sprite(ResourceManager.getInstance().getTexture("hp-bar-bg"));

                    m_state = InGame;
                    break;
                case InGame:
                    moveGameCharacters();

                    m_window.setCamera(m_defaultView);
                    m_currentLevel.processPlayer(m_player.getPosition());
                    m_currentLevel.render(m_window);

                    drawInGame(tx);

                    if (m_player.getHp() <= 0)
                        m_state = Loading;

                    if(BasicInput.isKeyPressed(BasicInput.Keys.Esc, true)) {
                        m_state = Paused;
                    }

                    break;
                case Paused:

                    if(BasicInput.isKeyPressed(BasicInput.Keys.Esc, true)) {
                        m_state = InGame;
                    }

                    //Pause Implementation

                    break;
                case Unloading:
                    m_window.close();
                    break;
            }

            m_window.renderFrame();
        }
    }

    private void loadResources() {
        ResourceManager.getInstance().processLoader();

        if(ResourceManager.getInstance().probeStatus()) {
            m_mainMenuBg = new Sprite(ResourceManager.getInstance().getTexture("mainMenu-bg"));
            m_mainMenuNg = new Sprite(ResourceManager.getInstance().getTexture("mainMenu-ng"));
            m_mainMenuLg = new Sprite(ResourceManager.getInstance().getTexture("mainMenu-lg"));
            m_mainMenuSt = new Sprite(ResourceManager.getInstance().getTexture("mainMenu-st"));
            m_mainMenuQt = new Sprite(ResourceManager.getInstance().getTexture("mainMenu-qt"));

            m_mainMenuNg.setPosition(100, -450);
            m_mainMenuLg.setPosition(100, -600);
            m_mainMenuSt.setPosition(100, -750);
            m_mainMenuQt.setPosition(100, -900);

            m_state = GameState.MainMenu;
        }
    }

    private void moveGameCharacters() {
        m_player.moved(m_defaultView);
        for(Monster monster : MonsterFactory.monsters)
            monster.moved();
    }

    private void drawInGame(Text tx) {
        m_window.draw(m_player);
        for(Monster monster : MonsterFactory.monsters)
            m_window.draw(monster);

        tx.setText(m_player.getName());
        tx.setPosition(m_player.getPosition().x - 730, -30);
        m_window.draw(tx);

        m_hp_bar_bg.setPosition(m_player.getPosition().x - 750, -60);
        m_window.draw(m_hp_bar_bg);

        m_hp_bar.setTextureRect(new Rectangle((int)(m_player.getHp()*m_hp_bar.getBounds().height / 50), (int)m_hp_bar.getBounds().height));
        m_hp_bar.setPosition(m_player.getPosition().x - 750, -60);
        m_window.draw(m_hp_bar);

        tx.setText((Integer.toString(m_player.getHp())));
        tx.setPosition(m_player.getPosition().x - 730, -60);
        m_window.draw(tx);
    }

    static private GameState m_state;
    private Window m_window;
    private Camera m_defaultView;
    private Level m_currentLevel;

    //Dump to Resources manager
    private Texture m_splashTexture;
    private Sprite m_splashSprite;
    private Player m_player;
    private Sprite m_hp_bar;
    private Sprite m_hp_bar_bg;

    //Menu
    private Sprite m_mainMenuBg;
    private Sprite m_mainMenuNg;
    private Sprite m_mainMenuLg;
    private Sprite m_mainMenuSt;
    private Sprite m_mainMenuQt;
}
