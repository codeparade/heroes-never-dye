package Engine.Renderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.GL_FUNC_ADD;
import static org.lwjgl.opengl.GL14.GL_FUNC_REVERSE_SUBTRACT;
import static org.lwjgl.opengl.GL14.GL_FUNC_SUBTRACT;

/**
 * Sekretne Tryby mieszania
 */

public class BlendMode {

    /**
     * Factor
     */

    enum Factor {
        Zero(GL_ZERO),
        One(GL_ONE),
        SrcColor(GL_SRC_COLOR),
        OneMinusSrcColor(GL_ONE_MINUS_SRC_COLOR),
        DstColor(GL_DST_COLOR),
        OneMinusDstColor(GL_ONE_MINUS_DST_COLOR),
        SrcAlpha(GL_SRC_ALPHA),
        OneMinusSrcAlpha(GL_ONE_MINUS_SRC_ALPHA),
        DstAlpha(GL_DST_ALPHA),
        OneMinusDstAlpha(GL_ONE_MINUS_DST_ALPHA);

        private int value;

        private Factor(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * Blending Equation
     */

    enum Equation {
        Add(GL_FUNC_ADD),                           // Pixel = Src * SrcFactor + Dst * DstFactor
        Subtract(GL_FUNC_SUBTRACT),                 //Pixel = Src * SrcFactor - Dst * DstFactor
        RevSubtract(GL_FUNC_REVERSE_SUBTRACT);       // Pixel = Dst * DstFactor - Src * SrcFactor

        private int value;

        private Equation(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    Factor colorSrcFactor = Factor.SrcAlpha;
    Factor colorDstFactor = Factor.OneMinusSrcAlpha;
    Equation colorEquation = Equation.Add;
    Factor alphaSrcFactor = Factor.One;
    Factor alphaDstFactor = Factor.OneMinusSrcAlpha;
    Equation alphaEquation = Equation.Add;

}
