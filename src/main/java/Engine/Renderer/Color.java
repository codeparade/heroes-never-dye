package Engine.Renderer;

/**
 * Color Class
 */

public class Color {

    /**
     * Constructor
     */

    public Color() {
        r = 1.f;
        g = 1.f;
        b = 1.f;
        a = 1.f;
    }

    /**
     * Constructor with initialization
     *
     * @param red Red Normalized Color Factor
     * @param green Green Normalized Color Factor
     * @param blue  Blue Normalized Color Factor
     * @param alpha Alpha Normalized Color Factor
     */

    public Color(float red, float green, float blue, float alpha) {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }

    /**
     * Red
     */

    public float r;

    /**
     * Green
     */

    public float g;

    /**
     * Blue
     */

    public float b;

    /**
     * Alpha Channel
     */

    public float a;

    /**
     * Predefined Colors
     */

    public static  Color Black = new Color(0.f,0.f,0.f,1.f);
    public static  Color Red = new Color(1.f,0.f,0.f,1.f);
    public static  Color Green = new Color(0.f,1.f,0.f,1.f);
    public static  Color Blue = new Color(0.f,0.f,1.f,1.f);
    public static  Color Magenta = new Color(1.f,0.f,1.f,1.f);
    public static  Color Yellow = new Color(1.f,1.f,0.f,1.f);
    public static  Color Cyan = new Color(0.f,1.f,1.f,1.f);
    public static  Color White = new Color(1.f,1.f,1.f,1.f);
    public static  Color Transparent = new Color(0.f,0.f,0.f,0.f);
}
