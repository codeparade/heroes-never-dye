package Engine.Renderer;

import Engine.System.Vector2Df;

/**
 * Text
 */

public class Text  extends Transformable implements Drawable{

    /**
     * Constructor
     */

    public Text() {
        m_text = "";
        m_charset = null;
        m_color = Color.White;
        m_vertices = new VertexArray();
    }

    /**
     * Constructor with initialization
     *
     * @param charset Font Texture
     * @param text  Text
     */

    public Text(String text, Texture charset) {
        m_text = text;
        m_charset = charset;
        m_color = Color.White;
        m_vertices = new VertexArray();
        update();
    }

    public void setText(String text) {
        m_text = text;
        update();
    }

    public void setCharSet(Texture charset) {
        m_charset = charset;
    }

    public void setColor(Color color) {
        m_color = color;
        update();
    }

    public String getString() {
        return m_text;
    }

    public Texture getCharset() {
        return  m_charset;
    }

    public Color getColor() {
        return m_color;
    }

    /**
     * Updates
     */

    private void update() {
        int x = 0;
        int y = 0;
        int idx = 0;
        int glyphSize = 64;

        m_vertices.clear();
        m_vertices.setType(PrimitiveType.Triangle);

        for (char c: m_text.toCharArray()) {
            if(c == '\n') {
                x = 0;
                y -= 32;
            }

            int xBase = (c % 16) * glyphSize;
            int yBase = (c / 16) * glyphSize;
            int margin = 2;

            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x, y), new Vector2Df(xBase, yBase - margin), m_color));
            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x, y+32), new Vector2Df( xBase , yBase + glyphSize + margin), m_color));
            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x+32, y ), new Vector2Df(xBase + glyphSize,  yBase - margin), m_color));
            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x+32, y+32), new Vector2Df(xBase + glyphSize, yBase + glyphSize + margin), m_color));
            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x+32, y), new Vector2Df(xBase + glyphSize,  yBase - margin), m_color));
            m_vertices.append(false, idx++, new Vertex(new Vector2Df(x, y+32), new Vector2Df(xBase,  yBase +glyphSize + margin), m_color));

            x+= 32;

        }

        m_vertices.update();
    }


    /**
     * Draw method
     *
     * @param tgt Render Target
     */

    public void draw(RenderTarget tgt) {
        RenderData data = new RenderData();

        if(m_charset != null) {
            data.transform = data.transform.combine(getTransform());
            data.texture = m_charset;
            tgt.draw(m_vertices, data);
        }

    }

    /**
     * Draw method
     *
     * @param tgt Render Target
     * @param data  Render Data
     */

    public void draw(RenderTarget tgt, RenderData data) {
        if(m_charset != null) {

            RenderData dt = new RenderData();
            dt.transform = data.transform.clone().combine((getTransform()));
            dt.texture = m_charset;
            tgt.draw(m_vertices, dt);
        }
    }

    private Texture m_charset;
    private String m_text;
    private Color m_color;
    private VertexArray m_vertices;
}
