package Engine.Renderer;

import Engine.System.Vector2Df;

/**
 * Vertex Class
 */

public class Vertex {

    /**
     * Constructor
     */

    public  Vertex() {
        position = new Vector2Df();
        textureCoord = new Vector2Df();
        color = new Color();
    }

    /**
     * Constructor with position initialization
     *
     *  @param pos Position of vertex
     */

    public  Vertex(Vector2Df pos) {
            position = pos;
            textureCoord = new Vector2Df();
            color = new Color();
    }

    /**
     * Constructor with pos / texture pos initialization
     *
     *  @param pos Position of vertex
     *  @param tex Texture coordinate in normalized space [0;1]
     */

    public  Vertex(Vector2Df pos, Vector2Df tex) {
        position = pos;
        textureCoord = tex;
        color = new Color();
    }

    /**
     *  Constructor with initialization of all members
     *
     *  @param pos Position of vertex
     *  @param tex Texture coordinate in normalized space [0;1]
     *  @param col Color of Vertex
     */

    public  Vertex(Vector2Df pos, Vector2Df tex, Color col) {
        this.position = pos;
        this.textureCoord = tex;
        this.color = col;
    }

    /**
     * Position
     */
    
    public Vector2Df position;

    /**
     * Texture Coordinate
     */

    public Vector2Df textureCoord;

    /**
     * Color
     */

    public Color color;
}
