package Engine.Renderer;

import Engine.System.Vector2Df;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Sprite
 */

public class Sprite extends Transformable implements Drawable  {

    /**
     * Constructor
     */

    public Sprite() {
        m_texture = null;
        m_rect = new Rectangle();
        m_vertices = new VertexArray(PrimitiveType.TriangleStrip, 4);
        m_isFlippedHorizontaly = false;
    }

    /**
     * Constructor with texture initialization
     */

    public Sprite(Texture tex) {
        m_texture = null;
        m_rect = new Rectangle();
        m_vertices = new VertexArray(PrimitiveType.TriangleStrip, 4);
        setTexture(tex);
        m_isFlippedHorizontaly = false;
    }

    /**
     * Set the texture
     *
     * @param t Texture to set
     */
    public void setTexture(Texture t) {
        if(m_texture == null)
            setTextureRect(new Rectangle(0,0,t.getSize().x, t.getSize().y));

        m_texture = t;

        update();
    }

    /**
     * Set texture rectangle
     *
     * @param rect Texture Rectangle
     */


    public  void setTextureRect(Rectangle rect) {
        if (rect != m_rect)
        {
            m_rect = rect;
            update();
        }
    }

    /**
     * Set Sprite color
     *
     * @param c  Color
     */


    public void  setColor(Color c) {
        m_vertices.get(0).color = c;
        m_vertices.get(1).color = c;
        m_vertices.get(2).color = c;
        m_vertices.get(3).color = c;

        m_vertices.update();
    }

    /**
     * Get Texture
     *
     * @return Texure
     */


    public  Texture getTexture() {
        return  m_texture;
    }

    /**
     * Get Color
     *
     * @return Color of sprite
     */

    public Color getColor() {
        return   m_vertices.get(0).color;
    }

    /**
     * Gets Texture Rectangle
     *
     * @return Texture Rectangle
     */

    public Rectangle2D getTextureRect() {
        return  m_rect;
    }

    /**
     * Gets Bounds
     *
     * @return Sprite Bounds
     */

    public Rectangle2D.Float getBounds() {
        float width = (float)Math.abs(m_rect.getWidth());
        float height = (float)Math.abs(m_rect.getHeight());

        Vector2Df pos = getPosition();
        return  new Rectangle2D.Float(pos.x, -pos.y, width,  height);
    }

    /**
     * Flip sprite horizontally
     * @param filp
     *
     */
    public void flipHorizontally(boolean flip) {
        m_isFlippedHorizontaly = flip;
    }

    /**
     * Gets horizontal flip status
     * @return  Is Flipped horizontally?
     *
     */

    public boolean getHorizontalFilip() {
        return  m_isFlippedHorizontaly;
    }

    /**
     * Draw method
     *
     * @param tgt Render Target
     */

    public void draw(RenderTarget tgt) {
        RenderData data = new RenderData();

        if(m_texture != null) {
            data.transform = data.transform.combine(getTransform());
            data.texture = m_texture;
            tgt.draw(m_vertices, data);
        }
    }

    /**
     * Draw method
     *
     * @param tgt Render Target
     * @param data  Render Data
     */

    public void draw(RenderTarget tgt, RenderData data) {

        if(m_texture != null) {

            RenderData dt = new RenderData();
            dt.transform = data.transform.clone().combine((getTransform()));
            dt.texture = m_texture;
            tgt.draw(m_vertices, dt);
        }
    }

    /**
     * Updates Sprite geometry
     *
     */

    private void update() {
        Rectangle2D.Float bounds = getBounds();

        if (m_vertices.getVertexCount() < 1)
        {
            m_vertices.append(false, 0, new Vertex(new Vector2Df(0.f, 0.f)));
            m_vertices.append(false, 1, new Vertex(new Vector2Df(0.f, bounds.height)));
            m_vertices.append(false, 2, new Vertex(new Vector2Df(bounds.width, 0.f)));
            m_vertices.append(false, 3, new Vertex(new Vector2Df(bounds.width, bounds.height)));
        }

        m_vertices.get(0).position = new Vector2Df(0.f, 0.f);
        m_vertices.get(1).position = new Vector2Df(0.f, bounds.height);
        m_vertices.get(2).position = new Vector2Df(bounds.width, 0.f);
        m_vertices.get(3).position = new Vector2Df(bounds.width, bounds.height);

        float top = (float)m_rect.getX();
        float left = (float)m_rect.getY();
        float bottom = top + (float)m_rect.getHeight();
        float right = left + (float)m_rect.getWidth();

        if(!m_isFlippedHorizontaly) {
            m_vertices.get(0).textureCoord = new Vector2Df(left, top);
            m_vertices.get(1).textureCoord = new Vector2Df(left, bottom);
            m_vertices.get(2).textureCoord = new Vector2Df(right, top);
            m_vertices.get(3).textureCoord = new Vector2Df(right, bottom);
        }
        else {
            m_vertices.get(0).textureCoord = new Vector2Df(right, top);
            m_vertices.get(1).textureCoord = new Vector2Df(right, bottom);
            m_vertices.get(2).textureCoord = new Vector2Df(left, top);
            m_vertices.get(3).textureCoord = new Vector2Df(left, bottom);
        }

        m_vertices.update();
    }

    /**
     * Vertices
     */

    private VertexArray m_vertices;

    /**
     * Texture
     */

    protected Texture m_texture;

    /**
     * Rectangle
     */

    private Rectangle2D m_rect;

    /**
     * Sprite flip horizontally
     */

    private boolean m_isFlippedHorizontaly;

}
