package Engine.Renderer;

import org.lwjgl.glfw.GLFWErrorCallback;

import static org.lwjgl.glfw.GLFW.*;
//import static org.lwjgl.glfw.GLFW.T;


/**
 * Static Library Initializer
 */

public class LibraryInit {

    /**
     * Ensures if library is initialized
     */

    public static void ensureInitLibrary()
    {
        if(isInitialized)
            return;

        GLFWErrorCallback.createPrint(System.err).set();

        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        isInitialized = true;
    }

    /**
     * Lib Status
     */

    private static boolean isInitialized = false;
}
