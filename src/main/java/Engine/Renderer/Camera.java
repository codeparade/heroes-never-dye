package Engine.Renderer;

import Engine.System.Vector2Df;

import java.awt.geom.Rectangle2D;

/**
 * Camera
 */

public class Camera {

    /**
     * Constructor
     */

    public Camera() {
        m_center = new Vector2Df(0.f,0.f);
        m_size = new Vector2Df(0.f,0.f);
        m_viewport = new Rectangle2D.Float(0.f,0.f,0.f,0.f);
        m_transform = new Transform();
        m_inverseTransform = new Transform();
        m_isInvTransformUpdateNeeded= true;
        m_isTransformUpdateNeeded = true;
    }

    /**
     * Constructor with initialization
     *
     * @param center  Center
     * @param size  Size
     */

    public Camera(Vector2Df size, Vector2Df center) {
        m_center = center;
        m_size = size;
        m_viewport = new Rectangle2D.Float(0.f,0.f, size.x, size.y);
        m_transform = new Transform();
        m_inverseTransform = new Transform();
        m_isInvTransformUpdateNeeded = true;
        m_isTransformUpdateNeeded = true;
    }

    /**
     * Sets Center
     *
     * @param x  Center X
     * @param y  Center Y
     */

    public void setCenter(float x, float y) {

            m_center.x = x;
            m_center.y = y;


        m_isTransformUpdateNeeded = true;
        m_isInvTransformUpdateNeeded = true;
    }

    /**
     * Sets Center
     *
     * @param center Center
     */

    public void setCenter(Vector2Df center) {
        setCenter(center.x, center.y);

    }

    /**
     * Gets Center
     *
     * @return  Center
     */

    public Vector2Df getCenter() {
        return  m_center;
    }

    /**
     * Sets Size
     *
     * @param w  Width
     * @param h Height
     */

    public void setSize(float w, float h) {
        m_size.x = w;
        m_size.y = h;

        setViewport(new Rectangle2D.Float(0.f,0.f, w, h));
        m_isTransformUpdateNeeded = true;
        m_isInvTransformUpdateNeeded = true;
    }


    /**
     * Sets Size
     *
     * @param size Size
     */

    public void setSize(Vector2Df size) {
        setSize(size.x, size.y);
    }


    /**
     * Gets Size
     *
     * @return Size
     */

    public Vector2Df getSize() {
        return  m_size;
    }

    /**
     * Sets Rotation
     *
     * @param angle Rotation
     *
     */

    public void setRotation(float angle) {
        m_rotation = angle % 360.f;

        if (m_rotation < 0)
            m_rotation += 360.f;

        m_isTransformUpdateNeeded = true;
        m_isInvTransformUpdateNeeded = true;
    }

    /**
     * Gets Rotaiton
     *
     * @return  Rotation
     */

    public float getRotation() {
        return  m_rotation;
    }

    /**
     * Resets
     *
     * @param rect Area
     */

    public void reset(Rectangle2D.Float rect) {
        m_center.x = rect.x + rect.width / 2.f;
        m_center.y = rect.y + rect.height / 2.f;

        m_size.x = rect.width;
        m_size.y = rect.height;

        m_rotation = 0;

        m_isTransformUpdateNeeded = true;
        m_isInvTransformUpdateNeeded = true;
    }

    /**
     * Moves Camera
     *
     * @param xOffset X Offset
     * @param yOffset  Y Offset
     */

    public void move(float xOffset, float yOffset) {
        setCenter(m_center.x + xOffset, m_center.y + yOffset);
    }

    /**
     * Moves Camera
     *
     * @param offset Offset
     */

    public void move(Vector2Df offset) {
        move(offset.x, offset.y);
    }

    /**
     * Rotates Camera
     *
     * @param angle Angle
     */

    public void rotate(float angle) {
        setRotation(angle + m_rotation);
    }

    /**
     * Zooms Camera
     *
     * @param factor Factor
     */

    public void zoom(float factor) {
        setSize(m_size.x * factor, m_size.y * factor);
    }

    /**
     * Sets Viewport
     *
     * @param vp Viewport
     */

    public void setViewport(Rectangle2D.Float vp) {
        m_viewport = vp;
    }

    /**
     * Gets Viewport
     *
     * @return  Viewport
     */

    public Rectangle2D.Float getViewport() {
        return  m_viewport;
    }

    /**
     * Gets Transform
     *
     * @return  Transform
     */

    public Transform getTransform() {
        if (m_isTransformUpdateNeeded)
        {
            float angle = m_rotation * 3.141592654f / 180.f;
            float cosine = (float)Math.cos(angle);
            float sine = (float)Math.sin(angle);
            float tx = -m_center.x * cosine - m_center.y * sine + m_center.x;
            float ty = m_center.x * sine - m_center.y * cosine + m_center.y;

            float a = 2.f / m_size.x;
            float b = -2.f / m_size.y;
            float c = -a * m_center.x;
            float d = -b * m_center.y;

            m_transform = new Transform(a * cosine, a * sine, a * tx + c,
                    -b * sine, b * cosine, b * ty + d,
                    0.f, 0.f, 1.f);

            m_isTransformUpdateNeeded = false;
        }

        return m_transform;
    }

    /**
     * Gets Inverse Transform
     *
     * @return  Inverse Transform
     */

    public Transform getInverseTransform() {

        if (m_isInvTransformUpdateNeeded)
        {
            m_inverseTransform = getTransform().getInverse();
            m_isInvTransformUpdateNeeded = false;
        }

        return m_inverseTransform;
    }

    /**
     * Center
     */


    private  Vector2Df m_center;

    /**
     * Size
     */


    private  Vector2Df m_size;

    /**
     * Rotation
     */


    private float m_rotation;

    /**
     * Viewport
     */

    private  Rectangle2D.Float m_viewport;

    /**
     * Transform
     */

    private  Transform m_transform;

    /**
     * Update Need
     */

    private  boolean m_isTransformUpdateNeeded;

    /**
     * Inv Transform
     */

    private  Transform m_inverseTransform;

    /**
     * Need Inv Update
     */

    private boolean m_isInvTransformUpdateNeeded;
}
