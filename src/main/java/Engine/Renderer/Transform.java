package Engine.Renderer;

import Engine.System.Vector2Df;
import javafx.scene.transform.MatrixType;

import java.lang.Math;

/**
 * Transform
 */

public class Transform implements Cloneable {

    /**
     * Constructor
     */

    public Transform() {
        matrix[0] = 1.f;		matrix[1] = 0.f;	matrix[2] = 0.f;	matrix[3] = 0.f;
        matrix[4] = 0.f;		matrix[5] = 1.f;	matrix[6] = 0.f;	matrix[7] = 0.f;
        matrix[8] = 0.f;		matrix[9] = 0.f;	matrix[10] = 1.f;	matrix[11] = 0.f;
        matrix[12] = 0.f;		matrix[13] = 0.f;	matrix[14] = 0.f;	matrix[15] = 1.f;
    }

    /**
     * Constructor with init
     *
     * @param a00 a00 Matrix Element
     * @param a01 a01 Matrix Element
     * @param a02 a02 Matrix Element
     * @param a10 a10 Matrix Element
     * @param a11 a11 Matrix Element
     * @param a12 a12 Matrix Element
     * @param a20 a20 Matrix Element
     * @param a21 a21 Matrix Element
     * @param a22 a22 Matrix Element
     */

    public Transform(float a00, float a01, float a02,
                     float a10, float a11, float a12,
                     float a20, float a21, float a22) {

        matrix[0] = a00; matrix[1] = a01; matrix[2] = 0.f; matrix[3] = a02;
        matrix[4] = a10; matrix[5] = a11; matrix[6] = 0.f; matrix[7] = a12;
        matrix[8] = 0.f; matrix[9] = 0.f; matrix[10] = 1.f; matrix[11] = 0.f;
        matrix[12] = a20; matrix[13] = a21; matrix[14] = 0.f; matrix[15] = a22;
    }

    /**
     * Gets Matrix
     *
     * @return Matrix
     */

    public float[] getMatrix() {
        return  matrix;
    }

    /**
     * Gets Inverse
     *
     * @return Inverse Transform
     */

    public Transform getInverse() {
        float determinat = matrix[0] * (matrix[15] * matrix[5] - matrix[7] * matrix[13]) -
                matrix[1] * (matrix[15] * matrix[4] - matrix[7] * matrix[12]) +
                matrix[3] * (matrix[13] * matrix[4] - matrix[5] * matrix[12]);

        if (determinat != 0.f)
        {
            return new Transform((matrix[15] * matrix[5] - matrix[7] * matrix[13]) / determinat,
                    -(matrix[15] * matrix[4] - matrix[7] * matrix[12]) / determinat,
                    (matrix[13] * matrix[4] - matrix[5] * matrix[12]) / determinat,
                    -(matrix[15] * matrix[1] - matrix[3] * matrix[13]) / determinat,
                    (matrix[15] * matrix[0] - matrix[3] * matrix[12]) / determinat,
                    -(matrix[13] * matrix[0] - matrix[1] * matrix[12]) / determinat,
                    (matrix[7] * matrix[1] - matrix[3] * matrix[5]) / determinat,
                    -(matrix[7] * matrix[0] - matrix[3] * matrix[4]) / determinat,
                    (matrix[5] * matrix[0] - matrix[1] * matrix[4]) / determinat);
        }
        else
        {
            return Identity;
        }
    }

    /**
     * Transforms Point
     *
     * @param point Point to transform
     *
     * @return Transformed Point
     */

    public Vector2Df transformPoint(Vector2Df point) {
        return new Vector2Df(matrix[0] * point.x + matrix[4] * point.y + matrix[12], matrix[1] * point.x + matrix[5] * point.y + matrix[13]);
    }

    /**
     * Combines Transforms
     *
     * @param t Transform to combine
     *
     * @return this*t
     */

    public Transform combine(Transform t) {
        float[] a = matrix;
        float[] b = t.getMatrix();
        float[] c = new float[16];

        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                for (int k = 0; k < 4; k++)
                    c[i*4 + j] += a[i*4 + k] * b[k*4 +j];

        this.matrix = c;
        return  this;
    }

    /**
     * Translates
     *
     * @param offset Offset to translate
     *
     * @return Transform
     */

    public Transform translate(Vector2Df offset) {
        Transform transl = new Transform(1, 0, offset.x,
                0, 1, -offset.y,
                0, 0, 1);

        return combine(transl);
    }

    /**
     * Rotates
     *
     * @param angle Angle (deg)
     * @param center Center of transform
     *
     * @return Transform
     */

    public Transform rotate(float angle, Vector2Df center) {
        float rad = angle * 3.141592654f / 180.f;
        float coss = (float)Math.cos(rad);
        float sinn = (float)Math.sin(rad);

        Transform rotation = new Transform(coss, -sinn, center.x* (1 - coss) + center.y * sinn,
                sinn, coss, center.y * (1 - coss) - center.x * sinn,
                0, 0, 1);

        return combine(rotation);
    }

    /**
     * Scales
     *
     * @param factors Factors
     * @param center Center of transform
     *
     * @return Transform
     */

    public Transform scale(Vector2Df factors, Vector2Df center) {
        Transform scaling = new Transform(factors.x, 0, center.x * (1 - factors.x),
                0, factors.y, center.y * (1 - factors.y),
                0, 0, 1);

        return combine(scaling);
    }

    public Transform clone() {
        Transform tRet = new Transform();
        tRet.matrix = matrix.clone();
        return  tRet;
    }

    /**
     * Identity Matrix
     */

    public static Transform Identity = new Transform();

    /**
     * Data
     */

    private float[] matrix = new float[16];
}
