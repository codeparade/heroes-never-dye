package Engine.Renderer;

import java.awt.*;

/**
 * Video Settings
 */

public class VideoSettings {

    /**
     * Constructor
     *
     * @param w Width
     * @param h  Height
     * @param bpp Bits per pixel
     */

    public VideoSettings(int w, int h, int bpp) {
        width = w;
        height = h;
        bitDepth = bpp;
    }

    /**
     * Gets Desktop Mode
     *
     * @return  Desktop Video Mode
     */

    public static VideoSettings getDesktopMode() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        return new VideoSettings(gd.getDisplayMode().getWidth(), gd.getDisplayMode().getHeight(),32);
    }


    /**
     * Width
     */

    public int width;

    /**
     * Height
     */

    public int height;

    /**
     * Bit Depth
     */

    public int bitDepth;
}
