package Engine.Renderer;

import Engine.System.Vector2Df;
import Engine.System.Vector2Di;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.opengl.ARBVertexArrayObject.glBindVertexArray;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL14.glBlendFuncSeparate;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindFragDataLocation;

/**
 * Render Target
 */

public class RenderTarget {

    /**
     * Constructor
     */

    public RenderTarget()  {
        m_ctxId = 0;
        m_cam = new Camera(new Vector2Df(1920,1080), new Vector2Df(960,540));
    }

    /**
     * Sets Active
     */

    public void setActive() {
        glfwMakeContextCurrent(m_ctxId);
    }

    /**
     * Sets clear color
     *
     * @param c Color
     */

    public void  setClearColor(Color c) {
        glClearColor(c.r,c.g,c.b,c.a);
    }

    /**
     * Clears Screen
     */

    public void clearScreen() {
        setActive();
        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_BLEND);
    }

    /**
     * Sets Camera
     *
     * @param cam  Camera to set
     */

    public void setCamera(Camera cam) {
        m_cam = cam;
    }

    /**
     * Gets Camera
     *
     * @return  Camera
     */

    public Camera getCamera() {
        return  m_cam;
    }

    /**
     * Maps Screen Pixel to Rendering Coordinates
     *
     * @param point Point on Screen
     *
     * @return Point in renderer space
     */

    public Vector2Df mapScreenPixelToRenderingCoords(Vector2Di point) {
        Rectangle2D.Float viewport = m_cam.getViewport();
        Vector2Df normalized = new Vector2Df(-1.f + 2.f * (point.x - viewport.x) / viewport.width,1.f - 2.f * (point.y - viewport.y) / viewport.height);
        return m_cam.getInverseTransform().transformPoint(normalized);
    }

    /**
     * Maps Screen Rendering Coords pixel to Screen Coordinates
     *
     * @param point Point in Renderer Space
     *
     * @return Point in window Space
     */

    public Vector2Di mapRenderingCoordsToScreenPixel(Vector2Df point) {
        Vector2Df normalized = m_cam.getTransform().transformPoint(point);

        int width = m_size.x;
        int height = m_size.y;
        Rectangle2D.Float viewportt = m_cam.getViewport();

        Rectangle viewport = new Rectangle((int)(0.5f + width  * viewportt.x), (int)(0.5f + height * viewportt.y), (int)(0.5f + width  * viewportt.width), (int)(0.5f + height * viewportt.height));

        return new Vector2Di((int)((normalized.x + 1.f) / 2.f * viewport.width + viewport.x) , (int)((-normalized.y + 1.f) / 2.f * viewport.height + viewport.y));
    }

    /**
     * Draws on target
     *
     * @param drawable Drawable to draw
     */

    public void draw(Drawable drawable) {
        drawable.draw(this);
    }

    /**
     * Draws on target
     *
     * @param drawable Drawable to draw
     * @param data Render Data
     */

    public void draw(Drawable drawable, RenderData data) {
        drawable.draw(this, data);
    }

    /**
     * Draws on target
     *
     * @param vbo Vertex Buffer Object
     * @param type Primitive Type
     * @param data Render Data
     */

    public void draw(VboImplementation vbo, PrimitiveType type, RenderData data) {
        setActive();

        //Blending
        glBlendFunc(data.blendMode.colorSrcFactor.getValue(),data.blendMode.colorDstFactor.getValue());
        glBlendEquationSeparate(data.blendMode.colorEquation.getValue(), data.blendMode.alphaEquation.getValue());

        //Shader Binding
        glUseProgram(data.shader.getShaderProgId());

        //Texture
        if(data.texture != null)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, data.texture.getTextureId());
            glUniform1i(glGetUniformLocation(data.shader.getShaderProgId(), "tSampler"), 0);

        }


        //BindUniforms
        glUniformMatrix4fv(glGetUniformLocation(data.shader.getShaderProgId(), "MV"), true, data.transform.getMatrix());
        glUniformMatrix4fv(glGetUniformLocation(data.shader.getShaderProgId(), "P"), true, m_cam.getTransform().getMatrix());

        //FragmentShader
        glUniform1i(glGetUniformLocation(data.shader.getShaderProgId(), "fTextured"), data.texture != null ? 1 : 0);
        glUniform1i(glGetUniformLocation(data.shader.getShaderProgId(), "fOverrideAlpha"), 0);
        glUniform1i(glGetUniformLocation(data.shader.getShaderProgId(), "fPixelsCoordinates"), data.texture != null ? data.texture.areCoordsNormalized() == true ? 0 : 1 : 0);
        glUniform1i(glGetUniformLocation(data.shader.getShaderProgId(), "fPixelsFlipped"), 0);

        //VAO and VBO
        glBindVertexArray(vbo.vaoId);
        glBindBuffer(GL_ARRAY_BUFFER, vbo.verticesBuff);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.indicesBuff);

        //Bind Attributes
        glEnableVertexAttribArray(glGetAttribLocation(data.shader.getShaderProgId(), "vVertex"));
        glVertexAttribPointer(glGetAttribLocation(data.shader.getShaderProgId(), "vVertex"), 2, GL_FLOAT, false, 32, 0);
        glEnableVertexAttribArray(glGetAttribLocation(data.shader.getShaderProgId(), "vTextureCoords"));
        glVertexAttribPointer(glGetAttribLocation(data.shader.getShaderProgId(), "vTextureCoords"), 2, GL_FLOAT, false, 32, 8);
        glEnableVertexAttribArray(glGetAttribLocation(data.shader.getShaderProgId(), "vColor"));
        glVertexAttribPointer(glGetAttribLocation(data.shader.getShaderProgId(), "vColor"), 4, GL_FLOAT, false, 32, 16);

        //Output
        glBindFragDataLocation(data.shader.getShaderProgId(), 0, "vFragColor");

        //Draw
        int modes[] = new int[] { GL_POINT, GL_LINE, GL_TRIANGLES, GL_TRIANGLE_STRIP};
        glDrawArrays(modes[type.ordinal()], 0, vbo.elementsCount);
    }

    /**
     * On Resize Callback
     *
     * @param w Width
     * @param h Height
     */

    protected  void onResize(int w, int h) {
        m_size = new Vector2Di(w,h);
        glViewport(0,0,w, h);
        m_cam.setViewport(new Rectangle2D.Float(0.f,0.f,(float)w,(float)h));
    }

    /**
     * Context Id
     */

    protected long m_ctxId;

    /**
     * Camera
     */

    private Camera m_cam;

    /**
     * Tgt Size
     */

    private Vector2Di m_size;
}
