package Engine.Renderer;

import Engine.System.Vector2Di;
import de.matthiasmann.twl.utils.PNGDecoder;
import org.lwjgl.opengl.GL;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

/**
 * Texture
 */

public class Texture {

    /**
     * Constructor
     */

    public Texture() {
        m_textureId = 0;
        m_normCoords = false;
        m_isRepeatMode = false;
        m_isSmooth = false;
        m_pixFlipped = false;
    }

    public Texture(String path) {
        m_textureId = 0;
        m_normCoords = false;
        m_isRepeatMode = false;
        m_isSmooth = false;
        m_pixFlipped = false;

        loadFromFile(path, new Rectangle());
    }

    /**
     * Creates Texture
     *
     * @param width  Width
     * @param height Height
     * @param bfr  Initial data for Texture
     */

    public void create(int width, int height, ByteBuffer bfr) {
        if (width <= 0 || height <= 0)
            return;

        Vector2Di act_size = new Vector2Di(OGLgetPowSize(width), OGLgetPowSize(height));

        int maxSiz = glGetInteger(GL_MAX_TEXTURE_SIZE);

        if (act_size.x > maxSiz || act_size.y > maxSiz)
            throw  new RuntimeException("Cant create texture: Problem with GPU");

        m_size = new Vector2Di(width, height);
        m_pixFlipped = false;

        LibraryInit.ensureInitLibrary();

        if (m_textureId == 0)
           m_textureId = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, m_textureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_isRepeatMode ? GL_REPEAT : GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_isRepeatMode ? GL_REPEAT : GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_size.x, m_size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, bfr);
    }

    /**
     * Loads from file
     *
     * @param fileName File
     * @param area  Area to load onto texture
     */

    public void loadFromFile(String fileName, Rectangle area) {
        try {

            InputStream in = new FileInputStream(fileName);
            PNGDecoder decoder = new PNGDecoder(in);

            ByteBuffer buf = ByteBuffer.allocateDirect(4*decoder.getWidth()*decoder.getHeight());
            decoder.decode(buf, decoder.getWidth()*4, PNGDecoder.Format.RGBA);

            buf.flip();
            in.close();

            create(decoder.getWidth(), decoder.getHeight(), null);

            if(area == null || area.width == 0 || area.height == 0)
                loadPixels(buf, decoder.getWidth(), decoder.getHeight(), 0, 0);
            else
                loadPixels(buf, area.width, area.height, area.x, area.y);

        }
        catch (IOException e) {
            System.out.println(fileName);
            throw new RuntimeException("Image Loading Problem");
        }
    }

    /**
     * Gets Size
     *
     * @return  Size
     */

    public Vector2Di getSize() {
        return  m_size;
    }

    /**
     * Load Pixels
     *
     * @param data  Data
     */

    public void loadPixels(ByteBuffer data) {
        loadPixels(data, m_size.x, m_size.y, 0, 0);
    }

    /**
     * Load Pixels
     *
     * @param data  Data
     * @param w  Width
     * @param h  Height
     * @param x  X offsett
     * @param y  Y offset
     */

    public void loadPixels(ByteBuffer data, int w, int h, int x, int y) {
        if (m_textureId != 0)
        {
            LibraryInit.ensureInitLibrary();

            data.flip();
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, m_textureId);

            glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);
            m_pixFlipped = false;
        }
    }

    /**
     * Setting Smooth Filtering
     *
     * @param filter Apply Filter?
     */

    public void setSmoothFiltering(boolean filter) {
        if (filter != m_isSmooth)
        {
           LibraryInit.ensureInitLibrary();

            m_isSmooth = filter;

            if (m_textureId != 0)
            {
                glBindTexture(GL_TEXTURE_2D, m_textureId);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);
            }
        }
    }

    /**
     * Is Smooth Filtering?
     *
     * @return Filter Satus
     */

    public boolean isSmoothFiltering() {
        return  m_isSmooth;
    }

    /**
     * Setting Repeating
     *
     * @param repeat Repeating
     */

    public void setRepeatingMode(boolean repeat) {
        if (repeat != m_isRepeatMode)
        {
            LibraryInit.ensureInitLibrary();

            m_isRepeatMode = repeat;

            if (m_textureId != 0)
            {
                glBindTexture(GL_TEXTURE_2D, m_textureId);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_isRepeatMode ? GL_REPEAT : GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_isRepeatMode ? GL_REPEAT : GL_CLAMP_TO_EDGE);
            }
        }
    }

    /**
     * Is Repeating Mode?
     *
     * @return  Repeating Status
     */

    public boolean isRepeatingMode() {
        return  m_isRepeatMode;
    }

    /**
     * Setting Normalization of coords
     *
     * @param normalized  Are coords normalized?
     */

    public void setCoodsNormalized(boolean normalized) {
        m_normCoords = normalized;
    }

    /**
     * Getting Coords Normalization
     *
     * @return  Are coords normalized?
     */

    public boolean areCoordsNormalized() {
        return  m_normCoords;
    }

    /**
     * Getting Texture Id
     *
     * @return  Texture ID
     */

    public int getTextureId() {
        return m_textureId;
    }

    /**
     * Getting OGL Pow Size (Internal Impl)
     *
     * @param size Size
     *
     * @return  Pow size
     */

    private int OGLgetPowSize(int size)
    {
        if (GL.getCapabilities().GL_ARB_texture_non_power_of_two) {
            return size;
        }
        else {
            int pow = 1;

            while (pow < size)
                pow *= 2;

            return pow;
        }
    }


    /**
     * Size
     */

    private Vector2Di m_size;

    /**
     * TID
     */

    private  int m_textureId;

    /**
     * Filtering
     */

    private boolean m_isSmooth;

    /**
     * Repeating
     */

    private boolean m_isRepeatMode;

    /**
     * Flipping
     */

    private boolean m_pixFlipped;

    /**
     * Coordinates Normalization
     */

    private boolean m_normCoords;
}
