package Engine.Renderer;

/**
 * VBO
 */

public class VboImplementation {

    /**
     * ID of VAO
     */

    int vaoId;

    /**
     * Vertices Buffer
     */

    int verticesBuff;

    /**
     * Indices Buffer
     */

    int indicesBuff;

    /**
     * Elements Count
     */

    int elementsCount;
}
