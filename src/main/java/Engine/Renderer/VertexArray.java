package Engine.Renderer;

import Engine.System.Vector2Df;
import org.lwjgl.BufferUtils;
import java.awt.geom.Rectangle2D;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

/**
 * Vertex Array
 */

public class VertexArray implements Drawable {

    /**
     * Constructor
     */

    public VertexArray() {
        m_pType = PrimitiveType.Triangle;
        m_indices = new ArrayList<>();
        m_vertices = new ArrayList<>();
        m_vboImp = new VboImplementation();

        m_vboImp.vaoId = glGenVertexArrays();
        m_vboImp.indicesBuff = glGenBuffers();
        m_vboImp.verticesBuff = glGenBuffers();
    }

    /**
     * Constructor with initialization
     *
     * @param type Type of primitives
     * @param vertexCount Vertices count
     */

    public VertexArray(PrimitiveType type, int vertexCount) {

        m_pType = type;
        m_indices = new ArrayList<>(vertexCount);
        m_vertices = new ArrayList<>(vertexCount);
        m_vboImp = new VboImplementation();

        m_vboImp.vaoId = glGenVertexArrays();
        m_vboImp.indicesBuff = glGenBuffers();
        m_vboImp.verticesBuff = glGenBuffers();
    }

    /**
     * Returns vertices count
     *
     * @return Vertex Count
     */

    public int getVertexCount() {
            return m_vertices.size();
    }

    /**
     * Clears the Vertex Array
     */

    public void clear() {
        m_vertices.clear();
        m_indices.clear();
    }

    /**
     * Resizes the Array
     *
     * @param vertexCount Vertices count
     */

    public void resize(int vertexCount) {
        m_vertices.ensureCapacity(vertexCount);
    }

    /**
     * Append Vertex
     *
     * @param existingVertex Does the vertex exists?
     * @param index Index of vertex
     * @param vtx Vertex to append
     */

    public void append(boolean existingVertex, int index, Vertex vtx) {
        if(!existingVertex)
            m_vertices.add(vtx);

        m_indices.add(index);
    }

    /**
     * Sets type of Primitive
     *
     * @param type  Type of primitive
     */

    public void setType(PrimitiveType type) {
        m_pType = type;
    }

    /**
     * Gets Type of primitive
     *
     * @return  Primitive Type
     */

    public PrimitiveType getType() {
        return  m_pType;
    }

    /**
     * Gets Bounds of Array
     *
     * @reutrn Bounds
     */

    public Rectangle2D.Float getBounds() {

        if(!m_vertices.isEmpty())
        {
            float left = m_vertices.get(0).position.x;
            float top = m_vertices.get(0).position.y;
            float right = m_vertices.get(0).position.x;
            float bottom = m_vertices.get(0).position.y;

            for (int i = 1; i < m_vertices.size(); ++i)
            {
                Vector2Df position = m_vertices.get(i).position;

                if (position.x < left)
                    left = position.x;
                else if (position.x > right)
                    right = position.x;

                if (position.y < top)
                    top = position.y;
                else if (position.y > bottom)
                    bottom = position.y;
            }

            return new Rectangle2D.Float(left, top, right - left, bottom - top);
        }
        else
        {
            return  new Rectangle2D.Float();
        }
    }

    /**
     * Updates Array
     */

    public void update() {
        m_vboImp.elementsCount = m_indices.size();

        glBindVertexArray(m_vboImp.vaoId);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboImp.verticesBuff);

        FloatBuffer bfr = BufferUtils.createFloatBuffer(32 * m_vertices.size());

        for(int i =0; i < m_vertices.size(); i++) {
            bfr.put(m_vertices.get(i).position.x);
            bfr.put(m_vertices.get(i).position.y);
            bfr.put(m_vertices.get(i).textureCoord.x);
            bfr.put(m_vertices.get(i).textureCoord.y);
            bfr.put(m_vertices.get(i).color.r);
            bfr.put(m_vertices.get(i).color.g);
            bfr.put(m_vertices.get(i).color.b);
            bfr.put(m_vertices.get(i).color.a);
        }

            bfr.flip();

            glBufferData(GL_ARRAY_BUFFER, bfr, GL_STATIC_DRAW);




        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    /**
     * Gets the Vertex
     *
     * @param idx  Index
     *
     * @reutrn Vtx
     */

    public Vertex get(int idx) {
        return  m_vertices.get(idx);
    }

    /**
     * Sets the Vertex
     *
     * @param idx  Index
     * @param v  Vertex
     */

    public void set(int idx, Vertex v) {
        m_vertices.set(idx, v);
    }

    /**
     * Draw
     *
     * @param tgt Rendering Target
     */

    public void draw(RenderTarget tgt) {
        tgt.draw(m_vboImp, m_pType, new RenderData());
    }

    /**
     * Draw
     *
     * @param tgt Rendering Target
     * @param data  RenderData
     */

    public void draw(RenderTarget tgt, RenderData data) {
        tgt.draw(m_vboImp, m_pType, data);
    }


    /**
     * Primitive Type
     */

    private PrimitiveType m_pType;

    /**
     * Indices
     */

    private ArrayList<Integer> m_indices;

    /**
     * Vertices
     */

    private ArrayList<Vertex> m_vertices;

    /**
     * VBO Implementation
     */

    private VboImplementation m_vboImp;

}
