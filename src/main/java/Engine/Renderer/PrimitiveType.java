package Engine.Renderer;

/**
 * Primitive Type
 */

public enum PrimitiveType {

    /**
     * Point
     */

    Point,

    /**
     * Line
     */

    Line,

    /**
     * Triangle
     */

    Triangle,

    /**
     * TriangleStrip
     */

    TriangleStrip
}
