package Engine.Renderer;

import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.*;

/**
 * Shader
 */

public class Shader {

    /**
     * Type
     */

    public enum Type {
        Vertex,
        Fragment
    }

    /**
     * Constructor
     */

    public Shader() {
        m_totalShaders = 0;
        m_shaderProgId = 0;
    }

    /**
     * Loads From String
     *
     * @param t  Type of shader
     * @param str  Data
     */

    public void loadFromString(Type t, String str) {

        int shid = glCreateShader(t == Type.Vertex ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER);

        glShaderSource(shid, str);
        glCompileShader(shid);
        int status = 0;
        status = glGetShaderi(shid, GL_COMPILE_STATUS);

        if(status != GL_TRUE)
        {

            throw  new RuntimeException(glGetShaderInfoLog(shid));
        }

        m_shaders[m_totalShaders++] = shid;

    }

    /**
     * Links shader
     */

    public void link() {
        m_shaderProgId = glCreateProgram();

        glBindAttribLocation(m_shaderProgId, 0, "vVertex");
        glBindAttribLocation(m_shaderProgId, 1, "vColor");
        glBindAttribLocation(m_shaderProgId, 2, "vTextureCoords");

        if(m_shaders[0] != 0)
         glAttachShader(m_shaderProgId, m_shaders[0]);

        if(m_shaders[1] != 0)
            glAttachShader(m_shaderProgId, m_shaders[1]);

        glLinkProgram(m_shaderProgId);

        glDeleteShader(m_shaders[0]);
        glDeleteShader(m_shaders[1]);
    }

    /**
     * Getting GL Program ID
     *
     * @return  Shader Program ID
     */

    public int getShaderProgId() {
        return  m_shaderProgId;
    }

    /**
     * ProgramId
     */

    private int m_shaderProgId;

    /**
     * Shaders Count
     */

    private int m_totalShaders;

    /**
     * Shaders array
     */

    private int m_shaders[] = new  int[2];

}
