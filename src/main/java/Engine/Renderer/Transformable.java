package Engine.Renderer;

import Engine.System.Vector2Df;

/**
 * Transformable
 */

public class Transformable {

    /**
     * Constructor
     */

    public Transformable() {
        m_center = new Vector2Df(0.f,0.f);
        m_position = new Vector2Df(0.f,0.f);
        m_scaling = new Vector2Df(1.f,1.f);
        m_transform = new Transform();
        m_inverseTransform = new Transform();
        m_isInverseTransformUpdateNeeded = true;
        m_isUpdateNeeded = true;
    }

    /**
     * Sets Position
     *
     * @param x x position
     * @param y  y Position
     */

    public void setPosition(float x, float y) {
        m_position.x = x;
        m_position.y = y;

        m_isUpdateNeeded = true;
        m_isInverseTransformUpdateNeeded = true;
    }

    /**
     * Sets Position
     *
     * @param pos Position
     */

    public void setPosition(Vector2Df pos) {
        setPosition(pos.x,pos.y);
    }

    /**
     * Gets Position
     *
     * @return  Position
     */

    public Vector2Df getPosition() {
        return  m_position;
    }

    /**
     * Sets Rotation
     *
     * @param angle Angle
     */

    public void setRotation(float angle) {
        m_rotation = (angle % 360.f);

        if (m_rotation < 0)
            m_rotation += 360;

        m_isUpdateNeeded = true;
        m_isInverseTransformUpdateNeeded = true;
    }

    /**
     * Gets Rotation
     *
     * @return  Rotation
     */

    public float getRotation() {
        return  m_rotation;
    }

    /**
     * Sets Scale
     *
     * @param xFactor X Factor
     * @param yFactor Y Factor
     */

    public void setScale(float xFactor, float yFactor) {
        m_scaling.x = xFactor;
        m_scaling.y = yFactor;

        m_isUpdateNeeded = true;
        m_isInverseTransformUpdateNeeded = true;
    }

    /**
     * Sets Scale
     *
     * @param factors Factors
     */

    public void setScale(Vector2Df factors) {
        setScale(factors.x, factors.y);
    }

    /**
     * Gets Scale
     *
     * @return  Scale
     */

    public Vector2Df getScale() {
        return  m_scaling;
    }

    /**
     * Sets Center Point
     *
     * @param x x position
     * @param y  y Position
     */

    public void setCenterPoint(float x, float y) {
        m_center.x = x;
        m_center.y = y;

        m_isUpdateNeeded = true;
        m_isInverseTransformUpdateNeeded = true;
    }

    /**
     * Sets Center Point
     *
     * @param pos Position
     */

    public void setCenterPoint(Vector2Df pos) {
        setCenterPoint(pos.x,pos.y);
    }

    /**
     * Gets Center Point
     *
     * @return  Center Point
     */

    public Vector2Df getCenterPoint() {
        return m_center;
    }

    /**
     * Moves
     *
     * @param xOffset Offset X
     * @param yOffset Offset Y
     */

    public void move(float xOffset, float yOffset){
        setPosition(m_position.x + xOffset, m_position.y + yOffset);
    }

    /**
     * Moves
     *
     * @param offset Offset
     */

    public void move(Vector2Df offset) {
        move(offset.x, offset.y);
    }

    /**
     * Rotates
     *
     * @param angle Angle
     */

    public void rotate(float angle) {
        setRotation(m_rotation + angle);
    }

    /**
     * Scales
     *
     * @param xFactor Factor X
     * @param yFactor Factor Y
     */

    public void scale(float xFactor, float yFactor) {
        setScale(m_scaling.x + xFactor, m_scaling.y + yFactor);
    }

    /**
     * Scales
     *
     * @param  factors Factors
     */

    public void scale(Vector2Df factors) {
        scale(factors.x, factors.y);
    }

    /**
     * Gets Transform
     *
     * @return  Transform
     */

    public Transform getTransform() {
        if (m_isUpdateNeeded)
        {
            float angle = -m_rotation * 3.141592654f / 180;
            float cosine = (float)Math.cos(angle);
            float sine = (float)Math.sin(angle);

            float sxc = m_scaling.x * cosine;
            float syc = m_scaling.y * cosine;
            float sxs = m_scaling.x * sine;
            float sys = m_scaling.y * sine;

            float tx = -m_center.x * sxc - m_center.y * sys + m_position.x;
            float ty = m_center.x * sxs - m_center.y * syc - m_position.y;

            m_transform = new Transform(sxc, sys, tx,
                    -sxs, syc, ty,
                    0.f, 0.f, 1.f);
            m_isUpdateNeeded = false;
        }

        return m_transform;
    }

    /**
     * Gets Inverse Transform
     *
     * @return  Inverse Transform
     */

    public Transform getInverseTransform() {
        if (m_isInverseTransformUpdateNeeded)
        {
            m_inverseTransform = getTransform().getInverse();

            m_isInverseTransformUpdateNeeded = false;
        }

        return m_inverseTransform;
    }

    /**
     * Center
     */

    private Vector2Df m_center;

    /**
     * Position
     */

    private Vector2Df m_position;

    /**
     * Rotation
     */

    private  float m_rotation;

    /**
     * Scaling
     */

    private  Vector2Df m_scaling;

    /**
     * Transform
     */

    private  Transform m_transform;

    /**
     * Update Need
     */

    private  boolean m_isUpdateNeeded;

    /**
     * Inv Transform
     */

    private  Transform m_inverseTransform;

    /**
     * Need Inv Update
     */

    private boolean m_isInverseTransformUpdateNeeded;
}
