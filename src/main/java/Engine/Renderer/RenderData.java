package Engine.Renderer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Renderer Data
 */

public class RenderData {

    /**
     * Constructor
     */

    public RenderData() {
        shader = new Shader();
        try {
            String fStr = new String(Files.readAllBytes(Paths.get("fragment.glsl")));
            String vStr = new String(Files.readAllBytes(Paths.get("vertex.glsl")));

            shader = new Shader();
            shader.loadFromString(Shader.Type.Vertex, vStr);
            shader.loadFromString(Shader.Type.Fragment, fStr);
            shader.link();
            transform = new Transform();
            texture = new Texture();
            blendMode = new BlendMode();
        }
        catch (IOException ex) {}
    }

    /**
     * Shader
     */

    public Shader shader;

    /**
     * Transform
     */

    public Transform transform;

    /**
     * Texture
     */

    public Texture texture;

    /**
     * Blending Mode
     */

    public BlendMode blendMode;
}
