package Engine.Renderer;

/**
 * Drawable
 */

public interface Drawable {

    /**
     * Draws the object on screen
     *
     * @param tgt Target to Render onto it
     */

    void draw(RenderTarget tgt);

    /**
     * Draws the object on screen
     *
     * @param tgt Target to Render onto it
     * @param rdata Render Data
     */

    void draw(RenderTarget tgt, RenderData rdata);
}

