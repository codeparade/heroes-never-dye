package Engine.Renderer;

import Engine.Audio.Context;
import Engine.Input.BasicInput;
import Engine.System.Vector2Di;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;


import static Engine.Renderer.LibraryInit.ensureInitLibrary;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

/**
 * Window
 */

public class Window extends  RenderTarget {

    /**
     * Constructor
     */

    public Window() {
        m_position = new Vector2Di(0,0);
        m_size = new Vector2Di(0,0);
        m_title = new String();
        m_isVisible = false;
        m_isVsync = false;
        m_isFullscreen = false;
    }

    /**
     * Constructor with creation
     *
     * @param sett VideoSettings
     * @param title  Title of Window
     */

    public Window(VideoSettings sett, String title) {
        m_position = new Vector2Di(0,0);
        m_size = new Vector2Di(0,0);
        m_title = new String();
        m_isVisible = false;
        m_isVsync = false;
        m_isFullscreen = false;

        create(sett, title);
    }

    /**
     * Creates Window
     *
     * @param sett VideoSettings
     * @param title  Title of Window
     */

    public void create(VideoSettings sett, String title) {

        ensureInitLibrary();

        m_ctxId = glfwCreateWindow(sett.width, sett.height, title, NULL, NULL);

        if ( m_ctxId == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        setActive();

        //OpenGL Init
        GL.createCapabilities();
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_STENCIL_TEST);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);

        setVSync(true);
        setVisibility(true);

        //Callbacks
        glfwSetWindowCloseCallback(m_ctxId, (window) -> {
            Context.cleanupAl();
            close();
        });

        glfwSetMouseButtonCallback(m_ctxId, GLFWMouseButtonCallback.create((window, button, action, mods) -> {
                BasicInput.mouseCallback(window,button,action,mods);
        }));

        glfwSetKeyCallback(m_ctxId, GLFWKeyCallback.create((window, key, scancode, action, mods) -> {
                BasicInput.keyboardCallback(window,key,scancode,action,mods);
        }));

        glfwSetWindowSizeCallback(m_ctxId, (long window, int width, int height) -> {
            onResize(width, height);
        });

        m_size = new Vector2Di(sett.width, sett.height);
        onResize(sett.width, sett.height);

        GLFWVidMode mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        setPosition(new Vector2Di((mode.width() - m_size.x)/ 2, (mode.height() - m_size.y)/ 2));
    }

    /**
     * Closes Window
     */

    public void close() {
        System.exit(0);
        glfwDestroyWindow(m_ctxId);
        m_ctxId = 0;
    }

    /**
     * Getting Window position
     *
     * @return  Position of Window
     */

    public Vector2Di getPosition() {
        int[] iks = new int[1];
        int[] igrek = new int[1];
        glfwGetWindowPos(m_ctxId,iks,igrek);
        return new Vector2Di(iks[0], igrek[0]);
    }

    /**
     * Setting Window position
     *
     * @param pos Position
     */

    public void setPosition(Vector2Di pos) {
        m_position = pos;
        glfwSetWindowPos(m_ctxId, pos.x, pos.y);
    }

    /**
     * Getting Window size
     *
     * @return  Size of Window
     */

    public Vector2Di getSize() {
        return  m_size;
    }

    /**
     * Setting Window size
     *
     * @param size Size of Window
     */

    public void  setSize(Vector2Di size) {
        m_size = size;
        glfwSetWindowSize(m_ctxId, size.x, size.y);
        onResize(size.x, size.y);
    }

    /**
     * Getting Window title
     *
     * @return  Title of Window
     */

    public String getTitle() {
        return  m_title;
    }

    /**
     * Setting Window title
     *
     * @param title Title of Window
     */

    public void setTitle(String title) {
        m_title = title;
        glfwSetWindowTitle(m_ctxId, title);
    }

    /**
     * Is Window Open?
     *
     * @return  Window status
     */

    public boolean isOpen() {
        return m_ctxId != 0;
    }

    /**
     * Renders frame
     */

    public void renderFrame() {
        glfwSwapBuffers(m_ctxId);
    }

    /**
     * Getting Window Visibility
     *
     * @return  Visibility of Window
     */

    public  boolean isVisible() {
        return  m_isVisible;
    }

    /**
     * Setting Window visibility
     *
     * @param visible Is Window Visible?
     */

    public void setVisibility(boolean visible) {
        m_isVisible = visible;

        if(m_isVisible)
            glfwShowWindow(m_ctxId);
        else
            glfwHideWindow(m_ctxId);
    }

    /**
     * Getting Window V-Sync
     *
     * @return  VSync Status
     */

    public boolean isVSync() {
        return  m_isVsync;
    }

    /**
     * Setting Window VSync
     *
     * @param vsync Vertical Sync
     */

    public  void setVSync(boolean vsync) {

        m_isVsync = vsync;
        glfwSwapInterval(vsync ? 1 : 0);

    }

    /**
     * Getting GLFW HWND
     *
     * @return  Handle
     */

    public long getHandle() {
        return  m_ctxId;
    }

    /**
     * Is Fullscreen?
     *
     * @return  FS status
     */

    public boolean isFullscreen() {
        return  m_isFullscreen;
    }

    /**
     * Setting Window Fullscreen
     *
     * @param fullscreen FS Status
     */

    public void setFulscreen(boolean fullscreen) {
        m_isFullscreen = fullscreen;
        GLFWVidMode mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        if (m_isFullscreen)
            glfwSetWindowMonitor(m_ctxId, glfwGetPrimaryMonitor(), 0, 0, mode.width(), mode.height(), mode.refreshRate());
        else {
            glfwSetWindowMonitor(m_ctxId, 0, m_position.x, m_position.y, m_size.x, m_size.y, mode.refreshRate());
            glfwSetWindowAttrib(m_ctxId, GLFW_DECORATED, GLFW_TRUE);
        }
    }

    /**
     * Size
     */

    private Vector2Di m_size;

    /**
     * Position
     */

    private Vector2Di m_position;

    /**
     * Title
     */

    private String m_title;

    /**
     * Visibility
     */

    private boolean m_isVisible;

    /**
     * VSync
     */

    private  boolean m_isVsync;

    /**
     * Fullscreen
     */

    private  boolean m_isFullscreen;

}
