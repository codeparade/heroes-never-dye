package Engine.Audio;

import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;
import static org.lwjgl.system.MemoryStack.stackMallocInt;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.libc.LibCStdlib.free;

/**
 * Sound
 */

public class Sound {

    /**
     * Constructor
     */

    public Sound() {
        m_bufferId = 0;
        m_srcId = 0;
        m_playing = false;
    }

    /**
     * Loads from OGG (Vorbis)
     *
     * @param path Path to file
     */

    public void loadFromOgg(String path) {

        Context.ensureCtxInit();

        IntBuffer chann = stackMallocInt(1);
        IntBuffer samplerate = stackMallocInt(1);

        ShortBuffer audioData;
        int channels;
        int sampleRate;

        try (MemoryStack stack = stackPush()) {
            //Allocate space to store return information from the function
            IntBuffer channelsBuffer   = stack.mallocInt(1);
            IntBuffer sampleRateBuffer = stack.mallocInt(1);

            audioData = stb_vorbis_decode_filename(path, channelsBuffer, sampleRateBuffer);

            channels = channelsBuffer.get(0);
            sampleRate = sampleRateBuffer.get(0);
        }

        int format = -1;
        if(channels == 1) {
            format = AL_FORMAT_MONO16;
        }
        else if(channels == 2) {
            format = AL_FORMAT_STEREO16;
        }


        m_bufferId = alGenBuffers();
        alBufferData(m_bufferId, format, audioData, sampleRate);

        free(audioData);

        m_srcId = alGenSources();
        alSourcei(m_srcId, AL_BUFFER, m_bufferId);


    }

    /**
     * Plays Sound
     */

    public void play() {
        if(m_playing)
           return;

        Context.ensureCtxInit();

        alSourcePlay(m_srcId);
        m_playing = true;
    }

    /**
     * Pauses Sound
     */

    public void pause() {
        if(!m_playing)
            return;


        Context.ensureCtxInit();

        alSourcePause(m_srcId);
        m_playing = false;
    }

    /**
     * Stops Sound
     */

    public void stop() {
        if(!m_playing)
            return;

        Context.ensureCtxInit();

        alSourceStop(m_srcId);
        m_playing = false;
    }

    /**
     * Destroys Sound
     */

    public void destroy() {
        Context.ensureCtxInit();

        alDeleteSources(m_srcId);
        m_srcId = 0;
        alDeleteBuffers(m_bufferId);
        m_bufferId = 0;
    }

    /**
     * Buffer ID
     */

    private int m_bufferId;

    /**
     * Source ID
     */

    private int m_srcId;

    /**
     * Status
     */

    private boolean m_playing;
}
