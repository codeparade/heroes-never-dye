package Engine.Audio;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;

import static org.lwjgl.openal.ALC10.*;

/**
 * Audio Context
 */

public class Context {

    /**
     * Ensures Context initialization
     */

    public static void ensureCtxInit() {
        if(ctxInit)
            return;

        String defaultDeviceName = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);
        device = alcOpenDevice(defaultDeviceName);

        int[] attributes = {0};
        context = alcCreateContext(device, attributes);
        alcMakeContextCurrent(context);

        ALCCapabilities alcCapabilities = ALC.createCapabilities(device);
        ALCapabilities alCapabilities = AL.createCapabilities(alcCapabilities);

        if(!alCapabilities.OpenAL10) {
            throw new  RuntimeException("OpenAL is not supported on this device!");
        }

        ctxInit = true;
    }

    /**
     * Cleaning up OpenAL
     */

    public static void cleanupAl() {
        if(!ctxInit)
            return;

        alcDestroyContext(context);
        alcCloseDevice(device);
    }

    /**
     * Initialized?
     */

    private static boolean ctxInit = false;

    /**
     * Audio Device
     */

    private static long device = 0;

    /**
     * Audio Context
     */

    private static long context = 0;
}
