package Engine.Input;

import Engine.Renderer.Window;
import Engine.System.Vector2Di;
import org.lwjgl.glfw.GLFWKeyCallback;

import java.awt.*;
import static org.lwjgl.glfw.GLFW.*;

/**
 * Basic Input: Keyboard and Mouse
 */

public class BasicInput {

    /**
     * Mouse Buttons
     */

    public enum MouseButton {
        Left,
        Right,
        Middle
    }

    /**
     * Keys
     */

    public enum Keys {
        Space(32),
        Apostrophe(39),
        Comma(44),
        Minus(45),
        Perion(46),
        Slash(47),
        Num0(48),
        Num1(49),
        Num2(50),
        Num3(51),
        Num4(52),
        Num5(53),
        Num6(54),
        Num7(55),
        Num8(56),
        Num9(57),
        Semicolon(59),
        Equal(61),
        A(65),
        B(66),
        C(67),
        D(68),
        E(69),
        F(70),
        G(71),
        H(72),
        I(73),
        J(74),
        K(75),
        L(76),
        M(77),
        N(78),
        O(79),
        P(80),
        Q(81),
        R(82),
        S(83),
        T(84),
        U(85),
        V(86),
        W(87),
        X(88),
        Y(89),
        Z(90),
        LeftBracket(91),
        Backslash(92),
        RightBracket(93),
        Tilde(96),
        Esc(256),
        Enter(257),
        Tab(258),
        Backspace(259),
        Insert(260),
        Delete(261),
        Right(262),
        Left(263),
        Down(264),
        Up(265),
        PgUp(266),
        PgDn(267),
        Home(268),
        End(269),
        CapsLock(280),
        ScrollLock(281),
        NumLock(282),
        PrtSc(283),
        Pause(284),
        F1(290),
        F2(291),
        F3(292),
        F4(293),
        F5(294),
        F6(295),
        F7(296),
        F8(297),
        F9(298),
        F10(299),
        F11(300),
        F12(301),
        NumPad0(320),
        NumPad1(321),
        NumPad2(322),
        NumPad3(323),
        NumPad4(324),
        NumPad5(325),
        NumPad6(326),
        NumPad7(327),
        NumPad8(328),
        NumPad9(329),
        NumPadDecimal(330),
        NumPadDiv(331),
        NumPadMul(332),
        NumPadSub(333),
        NumPadAdd(334),
        NumPadEnter(335),
        NumPadEqual(336), //On Apple Pro Keyboard
        LeftShift(340),
        LeftCtrl(341),
        LeftAlt(342),
        LeftSuper(343), //Command / WinKey
        RightShift(344),
        RightCtrl(345),
        RightAlt(346),
        RightSuper(347),
        Menu(348);

        private int value;

        private Keys(int value) {
            this.value = value;
        }

        private int getValue() {
            return value;
        }
    }

    /**
     * Is Keyboard button pressed?
     *
     * @param  key Key to check
     * @param  debounce Wait after key is pressed (Like Esc Menu in GTA5). Prevents single press detection.
     *
     *  @return True if key is pressed, false otherwise.
     */

    public static  boolean isKeyPressed(Keys key, boolean debounce) {
        boolean prs = keyboardState[key.getValue()];

        if(debounce && prs) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }
        }

        return  prs;
    }

    /**
     * Is Mouse button pressed?
     *
     * @param  button Button to check
     *
     *  @return True if button is pressed, false otherwise.
     */

    public static  boolean isMouseButtonPressed(MouseButton button) {
        return mouseButtons[button.ordinal()];
    }

    /**
     * Getting mouse position (absolute)
     *
     *  @return Mouse Position
     */

    public static Vector2Di getMousePosition() {
        Point p = MouseInfo.getPointerInfo().getLocation();
        return new Vector2Di(p.x, p.y);
    }

    /**
     * Getting mouse position (relative to window)
     *
     * @param win Window to calculate relative mouse position
     *
     *  @return Mouse Position
     */

    public static Vector2Di getMousePosition(Window win) {
        Point p = MouseInfo.getPointerInfo().getLocation();

        Vector2Di winPos = win.getPosition();
        return new Vector2Di( (p.x - winPos.x), (p.y - winPos.y));
    }

    /**
     * Processes Window Events
     */

    public static  void processEvents() {
        glfwPollEvents();
    }

    /**
     * Mouse Callback (Implementation only)
     *
     * @param window Window
     * @param button Button
     * @param action  Action
     * @param mods  Mods
     */

    public static void mouseCallback(long window, int button, int action, int mods) {
        switch (button) {
            case GLFW_MOUSE_BUTTON_LEFT:
                mouseButtons[MouseButton.Left.ordinal()] = (action == GLFW_PRESS ? true : false);
                break;
            case GLFW_MOUSE_BUTTON_RIGHT:
                mouseButtons[MouseButton.Right.ordinal()] = (action == GLFW_PRESS ? true : false);
                break;
            case GLFW_MOUSE_BUTTON_MIDDLE:
                mouseButtons[MouseButton.Middle.ordinal()] = (action == GLFW_PRESS ? true : false);
                break;
        }
    }

    /**
     * Keyboard Callback (Implementation only)
     *
     * @param window Window
     * @param key Button
     * @param scancode  ScanCode
     * @param action  Action
     * @param modes  Modes
     */

    public static  void keyboardCallback(long window, int key, int scancode, int action, int modes) {
        if(action == GLFW_PRESS || action == GLFW_REPEAT)
            keyboardState[key] = true;
        else
            keyboardState[key] = false;
    }

    /**
     * Mouse State Array
     */

    private static boolean[] mouseButtons = new boolean[MouseButton.values().length];

    /**
     * Keyboard State Array
     */

    private static boolean[] keyboardState = new boolean[65535];
}
