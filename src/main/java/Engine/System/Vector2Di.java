package Engine.System;

public class Vector2Di extends Vector2D<Integer> {

    public Vector2Di() {
        this.x = 0;
        this.y = 0;
    }

    public Vector2Di(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public float length() {
        return new Float(Math.sqrt(x*x+y*y));
    }
}