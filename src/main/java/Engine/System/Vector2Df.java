package Engine.System;

public class Vector2Df extends Vector2D<Float> {

    public Vector2Df() {
        this.x = 0.f;
        this.y = 0.f;
    }

    public Vector2Df(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float length() {
        return new Float(Math.sqrt(x*x+y*y));
    }
}
