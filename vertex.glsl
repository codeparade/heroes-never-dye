#version 330 core

layout (location = 0) in vec2 vVertex;
layout (location = 1) in vec4 vColor;
layout (location = 2) in vec2 vTextureCoords;


uniform mat4 MV;
uniform mat4 P;


smooth out vec2 vUV;
out vec4 fColor;

void main()
{
	mat4 MVP = P * MV;
	gl_Position =  MVP * vec4(vVertex.xy,0,1);
	vUV = vTextureCoords;
	fColor = vColor;	
}

